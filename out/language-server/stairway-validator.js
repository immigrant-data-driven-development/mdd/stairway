"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.StairwayValidator = exports.registerValidationChecks = void 0;
/**
 * Register custom validation checks.
 */
function registerValidationChecks(services) {
    const registry = services.validation.ValidationRegistry;
    const validator = services.validation.StairwayValidator;
    const checks = {};
    registry.register(checks, validator);
}
exports.registerValidationChecks = registerValidationChecks;
/**
 * Implementation of custom validations.
 */
class StairwayValidator {
}
exports.StairwayValidator = StairwayValidator;
//# sourceMappingURL=stairway-validator.js.map
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createStairwayServices = exports.StairwayModule = void 0;
const langium_1 = require("langium");
const stairway_scope_1 = require("./stairway-scope");
const module_1 = require("./generated/module");
const stairway_validator_1 = require("./stairway-validator");
/**
 * Dependency injection module that overrides Langium default services and contributes the
 * declared custom services. The Langium defaults can be partially specified to override only
 * selected services, while the custom services must be fully specified.
 */
exports.StairwayModule = {
    references: {
        ScopeComputation: (services) => new stairway_scope_1.CustomScopeComputation(services)
    },
    validation: {
        StairwayValidator: () => new stairway_validator_1.StairwayValidator()
    }
};
/**
 * Create the full set of services required by Langium.
 *
 * First inject the shared services by merging two modules:
 *  - Langium default shared services
 *  - Services generated by langium-cli
 *
 * Then inject the language-specific services by merging three modules:
 *  - Langium default language-specific services
 *  - Services generated by langium-cli
 *  - Services specified in this file
 *
 * @param context Optional module context with the LSP connection
 * @returns An object wrapping the shared services and the language-specific services
 */
function createStairwayServices(context) {
    const shared = (0, langium_1.inject)((0, langium_1.createDefaultSharedModule)(context), module_1.StairwayGeneratedSharedModule);
    const Stairway = (0, langium_1.inject)((0, langium_1.createDefaultModule)({ shared }), module_1.StairwayGeneratedModule, exports.StairwayModule);
    shared.ServiceRegistry.register(Stairway);
    (0, stairway_validator_1.registerValidationChecks)(Stairway);
    return { shared, Stairway };
}
exports.createStairwayServices = createStairwayServices;
//# sourceMappingURL=stairway-module.js.map
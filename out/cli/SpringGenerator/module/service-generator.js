"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.generateService = void 0;
const langium_1 = require("langium");
function generateService(localEntity, functionMapping, package_name, util_package) {
    var _a;
    return (0, langium_1.expandToStringWithNL) `

    package ${package_name}.services;
    import ${(_a = localEntity.ontology) === null || _a === void 0 ? void 0 : _a.module}.${localEntity.name};
    ${localEntity.relations.map(relation => { var _a; return `import <todo>.exceptions.${(_a = relation.type.ref) === null || _a === void 0 ? void 0 : _a.name}ExceptionNotFound`; }).join(';\n')}
    import ${package_name}.applications.${localEntity.name}Application;
    import ${package_name}.mappers.${functionMapping.name}Mapper;
    import ${util_package}.Mapper;

    import lombok.RequiredArgsConstructor;
    import lombok.extern.slf4j.Slf4j;
    import org.apache.kafka.clients.consumer.ConsumerRecord;
    import org.springframework.beans.factory.annotation.Autowired;
    import org.springframework.beans.factory.annotation.Value;
    import org.springframework.stereotype.Component;

    @Slf4j
    @RequiredArgsConstructor
    @Component
    public class ${localEntity.name}Service {

        
        @Autowired
        private ${localEntity.name}Application application;        

        public void process(ConsumerRecord<String, String> payload,Mapper<${localEntity.name}> mapper) throws ${localEntity.relations.map(relation => { var _a; return `${(_a = relation.type.ref) === null || _a === void 0 ? void 0 : _a.name}ExceptionNotFound`; }).join(',')} Exception{

           
            ${localEntity.name} instance = mapper.map(payload.value());
            Boolean exists = application.exists(instance.getInternalId());
            if (!exists){
                ${localEntity.relations.length > 0 ? `application.create (instance, payload.value());` : `application.create (instance);`}
                
            }
            
        }

    }
    `;
}
exports.generateService = generateService;
//# sourceMappingURL=service-generator.js.map
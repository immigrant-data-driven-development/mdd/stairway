"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.mongoExceptionGenerator = exports.generateClassException = void 0;
const langium_1 = require("langium");
function generateClassException(cls, package_name) {
    return (0, langium_1.expandToStringWithNL) `
    package ${package_name}.exceptions;
  
    public class ${cls.name}ExceptionNotFound extends RuntimeException{
  
      public ${cls.name}ExceptionNotFound(String message) {
          super(message);
      }
  }
  
    `;
}
exports.generateClassException = generateClassException;
function mongoExceptionGenerator(path_package) {
    return (0, langium_1.expandToStringWithNL) `
    package ${path_package};
  
    public class MongoNotFound extends RuntimeException{
  
      public MongoNotFound(String message) {
          super(message);
      }
    }
    `;
}
exports.mongoExceptionGenerator = mongoExceptionGenerator;
//# sourceMappingURL=exception-generator.js.map
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.stringUtilGenerator = exports.jsonUtilGenerator = exports.dataUtilGenerator = exports.dataSearchGenerator = void 0;
const langium_1 = require("langium");
function dataSearchGenerator(path_package) {
    return (0, langium_1.expandToStringWithNL) `
    package ${path_package};
  
    import lombok.Data;
    import lombok.NoArgsConstructor;
    import lombok.AllArgsConstructor;
    import lombok.experimental.SuperBuilder;
  
    @Data
    @SuperBuilder
    @NoArgsConstructor
    @AllArgsConstructor
    public class DataSearch {
  
        private Long elementValue;
  
        private String table;
  
        private String database;
  
        public String toString (){
  
            return "elementValue: "+elementValue + " - Table:"+table+" - Database: "+database;
  
        }
    }
    `;
}
exports.dataSearchGenerator = dataSearchGenerator;
function dataUtilGenerator(package_name) {
    return (0, langium_1.expandToStringWithNL) `
    package ${package_name};
    
    import java.time.LocalDate;
    import java.time.LocalDateTime;
    import java.time.format.DateTimeFormatter;
    import java.time.LocalDateTime;
    import java.time.ZonedDateTime;
    public class DateUtil {
  
  
      public  static LocalDate createLocalDate(String day, String month, String year){
  
          month = checkMonthAndDay(month);
          day = checkMonthAndDay(day);
  
          String date = year+"-"+month+"-"+day;
          return  LocalDate.parse(date);
  
  
      }
  
      public static LocalDateTime createLocalDateTime (String date) {
          DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSX");
  
          return  LocalDateTime.parse(date, formatter);
      }
  
      public static LocalDateTime createLocalDateTimeZ (String date) {
          DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE_TIME;
          ZonedDateTime zonedDateTime = ZonedDateTime.parse(date, formatter);
          return zonedDateTime.toLocalDateTime();
  
  
      }
  
      private static String checkMonthAndDay(String value){
          return value.length() == 2? value: "0"+value;
      }
  }
    `;
}
exports.dataUtilGenerator = dataUtilGenerator;
function jsonUtilGenerator(package_name) {
    return (0, langium_1.expandToStringWithNL) `
    package ${package_name};
    import ${package_name}.mongo.DataSearch;
    import com.fasterxml.jackson.core.JsonProcessingException;
    import com.fasterxml.jackson.databind.JsonNode;
    import com.fasterxml.jackson.databind.ObjectMapper;
    import lombok.extern.slf4j.Slf4j;
  
    @Slf4j
    public class JsonUtil {
  
        private static JsonNode create (String payload) throws JsonProcessingException {
            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.readTree(payload);
        }
  
        public static DataSearch retrieve (String payload, String table, String elementName) throws JsonProcessingException {
            JsonNode rootNode = create(payload);
            Long elementValue = rootNode.path("payload").path("after").path(elementName).asLong();
            String database = rootNode.path("payload").path("source").path("db").asText();
  
            return DataSearch.builder().elementValue(elementValue).table(table).database(database).build();
        }
  
        public static String retrieveInternalID (String payload) throws JsonProcessingException {
            JsonNode rootNode = create(payload);
            return rootNode.path("payload").path("after").path("internal_id").asText();
        }
  
    }
    `;
}
exports.jsonUtilGenerator = jsonUtilGenerator;
function stringUtilGenerator(package_name) {
    return (0, langium_1.expandToStringWithNL) `
    package ${package_name};
    import java.time.LocalDate;
  
    public class StringUtil {
  
        public  static String check (String value){
  
            if (!value.isEmpty() || !value.isBlank() || value != null){
                return value;
            }
            return "";
  
        }
  
    }
    `;
}
exports.stringUtilGenerator = stringUtilGenerator;
//# sourceMappingURL=util-generator.js.map
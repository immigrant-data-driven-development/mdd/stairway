"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.generateFilter = void 0;
const langium_1 = require("langium");
function generateFilter(localEntity, functionMapping, package_name) {
    return (0, langium_1.expandToStringWithNL) `
    package ${package_name}.filters;

    import com.fasterxml.jackson.databind.JsonNode;
    import com.fasterxml.jackson.databind.ObjectMapper;
    import org.springframework.stereotype.Component;

    import java.util.Map;

    @Component
    public class ${functionMapping.name}Filter {

    public Boolean isValid (String element) throws Exception {

        ${(0, langium_1.toString)(generateFilterConditions(functionMapping))}
    }
}`;
}
exports.generateFilter = generateFilter;
function generateFilterConditions(functionMapping) {
    return (0, langium_1.expandToStringWithNL) `
    ${functionMapping.whenmappers.length ? generateExpression(functionMapping) : `return true;`}
    `;
}
function generateExpression(functionMapping) {
    return (0, langium_1.expandToStringWithNL) `
    ObjectMapper objectMapper = new ObjectMapper();

    JsonNode rootNode = objectMapper.readTree(element);

    return (${functionMapping.whenmappers.map(when => generateCondition(when)).join(`&&\n`)})? true : false;`;
}
function generateCondition(whenMapper) {
    return (0, langium_1.expandToStringWithNL) `
rootNode.${whenMapper.source.split(".").flatMap(element => `path("${element}")`).join(".")}.asText().equals("${whenMapper.value}")`;
}
//# sourceMappingURL=filter-generator.js.map
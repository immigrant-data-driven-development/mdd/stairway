"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.generateMapper = void 0;
const langium_1 = require("langium");
function generateMapper(localEntity, functionMapping, package_name, util_package) {
    var _a;
    return (0, langium_1.expandToStringWithNL) `
    package ${package_name}.mappers;

    import ${(_a = localEntity.ontology) === null || _a === void 0 ? void 0 : _a.module}.${localEntity.name};
    import ${util_package}.Mapper;
    import ${util_package}.DateUtil;
    import ${util_package}.StringUtil;

    import org.springframework.boot.json.JsonParser;
    import org.springframework.boot.json.JsonParserFactory;
    import org.springframework.stereotype.Component;
    import java.time.LocalDateTime;
    import com.fasterxml.jackson.databind.JsonNode;
    import com.fasterxml.jackson.databind.ObjectMapper;

    @Component
    public class ${functionMapping.name}Mapper  implements Mapper <${localEntity.name}>{

    public ${localEntity.name} map (String element) throws Exception {

        ObjectMapper objectMapper = new ObjectMapper();

        JsonNode rootNode = objectMapper.readTree(element);

        ${functionMapping.attributemappers.map(att => createAttributesMappers(att)).join("\n")}
        
        return ${localEntity.name}.builder().${functionMapping.attributemappers.map(att => { var _a, _b; return `${(_a = att.target.ref) === null || _a === void 0 ? void 0 : _a.name.toLocaleLowerCase()}(${(_b = att.target.ref) === null || _b === void 0 ? void 0 : _b.name.toLocaleLowerCase()})`; }).join(".\n")}.build();
    }
}`;
}
exports.generateMapper = generateMapper;
function createAttributesMappers(att) {
    var _a;
    return (0, langium_1.expandToStringWithNL) `
    ${createAttributesType(att)} ${(_a = att.target.ref) === null || _a === void 0 ? void 0 : _a.name.toLocaleLowerCase()} ${parserMapping(att)}
    `;
}
function entireCommnad(att) {
    return (0, langium_1.expandToString) `rootNode.${att.source.split(".").flatMap(element => `path("${element}")`).join(".")}.asText()`;
}
function createAttributesType(att) {
    var _a;
    switch ((_a = att.target.ref) === null || _a === void 0 ? void 0 : _a.type.toLowerCase()) {
        case "date": {
            return "LocalDateTime";
        }
        case "long": {
            return "Long";
        }
        case "integer": {
            return "Integer";
        }
        default: {
            return "String";
        }
    }
}
function parserMapping(att) {
    var _a;
    switch ((_a = att.target.ref) === null || _a === void 0 ? void 0 : _a.type.toLowerCase()) {
        case "date": {
            return (0, langium_1.expandToString) ` = DateUtil.createLocalDateTimeZ(${entireCommnad(att)});`;
        }
        case "long": {
            return (0, langium_1.expandToString) `= Long.parseLong(${entireCommnad(att)});`;
        }
        case "integer": {
            return (0, langium_1.expandToString) ` = Integer.parseInt(${entireCommnad(att)});`;
        }
        default: {
            return (0, langium_1.expandToString) ` = StringUtil.check(${entireCommnad(att)});`;
        }
    }
}
//# sourceMappingURL=mapper-generator.js.map
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.applicationAbstractGenerator = exports.mongoApplicationGenerator = exports.generateClassApplication = void 0;
const langium_1 = require("langium");
function generateConditionsCreateFunctionApplication(relation) {
    var _a, _b, _c, _d, _e, _f, _g, _h;
    return (0, langium_1.expandToStringWithNL) `
    if (${(_a = relation.type.ref) === null || _a === void 0 ? void 0 : _a.name.toLocaleLowerCase()}DataSearch.getElementValue() !=null && ${(_b = relation.type.ref) === null || _b === void 0 ? void 0 : _b.name.toLocaleLowerCase()}DataSearch.getElementValue() != 0){
      Document document = this.retrieveDocument(${(_c = relation.type.ref) === null || _c === void 0 ? void 0 : _c.name.toLocaleLowerCase()}DataSearch);
      String internalID = JsonUtil.retrieveInternalID(document.toJson());
      ${(_d = relation.type.ref) === null || _d === void 0 ? void 0 : _d.name} ${(_e = relation.type.ref) === null || _e === void 0 ? void 0 : _e.name.toLocaleLowerCase()} = this.${(_f = relation.type.ref) === null || _f === void 0 ? void 0 : _f.name.toLocaleLowerCase()}Application.retrieveByInternalID(internalID);
      instance.set${(_g = relation.type.ref) === null || _g === void 0 ? void 0 : _g.name.toLocaleLowerCase()}(${(_h = relation.type.ref) === null || _h === void 0 ? void 0 : _h.name.toLocaleLowerCase()});
  
    }
    `;
}
function generateCreateFunctionApplication(cls) {
    return (0, langium_1.expandToStringWithNL) `
     
    public  ${cls.name} create( ${cls.name} instance, String payload) throws Exception, MongoNotFound ${cls.relations.map(relation => { var _a; return `${(_a = relation.type.ref) === null || _a === void 0 ? void 0 : _a.name}ExceptionNotFound`; }).join(`,`)}{
      ${cls.relations.map(relation => { var _a, _b, _c; return `DataSearch ${(_a = relation.type.ref) === null || _a === void 0 ? void 0 : _a.name.toLocaleLowerCase()}DataSearch = JsonUtil.retrieve(payload,"${(_b = relation.type.ref) === null || _b === void 0 ? void 0 : _b.name.toLocaleLowerCase()}","${(_c = relation.type.ref) === null || _c === void 0 ? void 0 : _c.name.toLocaleLowerCase()}_id");`; }).join(`\n`)}
  
      ${cls.relations.map(relation => generateConditionsCreateFunctionApplication(relation)).join("\n")}
      
      return this.create(instance);
    }
    `;
}
function generateApplicationtoApplication(relation) {
    var _a, _b;
    return (0, langium_1.expandToStringWithNL) `
    @Autowired
    private ${(_a = relation.type.ref) === null || _a === void 0 ? void 0 : _a.name}Application ${(_b = relation.type.ref) === null || _b === void 0 ? void 0 : _b.name.toLocaleLowerCase()}Application;
    `;
}
function generateClassApplication(localEntity, package_name, package_util) {
    var _a, _b;
    return (0, langium_1.expandToStringWithNL) `
      package ${package_name}.applications;
      
      import ${(_a = localEntity.ontology) === null || _a === void 0 ? void 0 : _a.module}.${localEntity.name};
      import ${(_b = localEntity.ontology) === null || _b === void 0 ? void 0 : _b.module.replace('models', 'repositories')}.${localEntity.name}Repository;
      ${localEntity.relations.map(relation => { var _a; return `import <todo>.exceptions.${(_a = relation.type.ref) === null || _a === void 0 ? void 0 : _a.name}ExceptionNotFound`; }).join(';\n')}
      import ${package_util}.ApplicationAbstract;
      import ${package_util}.mongo.MongoNotFound;
      import ${package_name}.exceptions.${localEntity.name}ExceptionNotFound;
      import org.springframework.stereotype.Component;
      import org.springframework.transaction.annotation.Transactional;
      import java.util.Optional;
      import org.springframework.beans.factory.annotation.Autowired;
      import org.bson.Document;
      import br.nemo.immigrant.ontology.entity.eo.teams.repositories.projections.IDProjection;
  
      @Component
      @Transactional
      public class ${localEntity.name}Application extends ApplicationAbstract  {   
      
      @Autowired
      private ${localEntity.name}Repository repository;
  
      ${localEntity.relations.map(relation => generateApplicationtoApplication(relation)).join("\n")}
  
  
      public ${localEntity.name} create(${localEntity.name} instance) {
        return this.repository.save(instance);
      }
  
      ${generateCreateFunctionApplication(localEntity)} 
  
  
      public ${localEntity.name} retrieveByExternalID(String externalID) throws ${localEntity.name}ExceptionNotFound {
          Optional<IDProjection> result = this.repository.findByExternalId(externalID);
  
          IDProjection projection = result.orElseThrow(() -> new ${localEntity.name}ExceptionNotFound(externalID));
  
          return createInstance(projection);
      }
  
      public ${localEntity.name} retrieveByInternalID(String internalID) throws ${localEntity.name}ExceptionNotFound {
          Optional<IDProjection> result = this.repository.findByInternalId(internalID);
  
          IDProjection projection = result.orElseThrow(() -> new ${localEntity.name}ExceptionNotFound(internalID));
  
          return createInstance(projection);
      }
  
      private ${localEntity.name} createInstance(IDProjection projection){
          return  ${localEntity.name}.builder().id(projection.getId()).externalId(projection.getExternalId()).internalId(projection.getInternalId()).name(projection.getName()).build();
      }
  
      public Boolean exists (String internalID){
          return this.repository.existsByInternalId(internalID);
      }
  
    }
    `;
}
exports.generateClassApplication = generateClassApplication;
function mongoApplicationGenerator(path_package) {
    return (0, langium_1.expandToStringWithNL) `
    package ${path_package};
  
    import lombok.extern.slf4j.Slf4j;
    import org.springframework.stereotype.Component;
    import org.springframework.transaction.annotation.Transactional;
    import org.bson.Document;
    import org.springframework.data.mongodb.core.query.BasicQuery;
    import org.springframework.beans.factory.annotation.Autowired;
    import org.springframework.data.mongodb.core.MongoTemplate;
    import java.util.List;
  
    @Component
    @Transactional
    @Slf4j
    public class MongoApplication {
    
        @Autowired
        private MongoTemplate mongoTemplate;
    
        public static final String COLLECTION = "input";
        public List<Document>  find(String tableName, Long id, String database) throws MongoNotFound{
    
            String jsonQuery = "{$and:[{\\"payload.source.table\\":\\"{TABLE}\\"},{\\"payload.after.id\\":{ID}},{\\"payload.source.db\\":\\"{DATABASE}\\"}]}";
    
            jsonQuery = jsonQuery.replace("{TABLE}", tableName);
            jsonQuery = jsonQuery.replace("{ID}", String.valueOf(id));
            jsonQuery = jsonQuery.replace("{DATABASE}", database);
    
            log.info(" jsonQuery: {}", jsonQuery);
    
            BasicQuery query = new BasicQuery(jsonQuery);
            List<Document> documents = mongoTemplate.find(query, Document.class, COLLECTION);
    
            if (documents.isEmpty()) throw new MongoNotFound(tableName+"-"+id+"-"+database);
    
            return documents;
        }
    }
    `;
}
exports.mongoApplicationGenerator = mongoApplicationGenerator;
function applicationAbstractGenerator(package_name) {
    return (0, langium_1.expandToStringWithNL) `
    package ${package_name};
  
    import ${package_name}.mongo.MongoApplication;
    import ${package_name}.mongo.MongoNotFound;
    import ${package_name}.mongo.DataSearch;
    import org.springframework.beans.factory.annotation.Autowired;
    import org.bson.Document;
    import java.util.List;
  
    public abstract class ApplicationAbstract {
  
        @Autowired
        private MongoApplication mongoApplication;
  
        protected Document retrieveDocument(DataSearch data) throws MongoNotFound {
            List<Document> documents = mongoApplication.find(data.getTable(),
                    data.getElementValue(),
                    data.getDatabase());
            return documents.get(0);
        }
    }
    `;
}
exports.applicationAbstractGenerator = applicationAbstractGenerator;
//# sourceMappingURL=application-generator.js.map
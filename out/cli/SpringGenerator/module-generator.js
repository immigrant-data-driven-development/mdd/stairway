"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.generateModules = void 0;
const path_1 = __importDefault(require("path"));
const fs_1 = __importDefault(require("fs"));
const ast_1 = require("../../language-server/generated/ast");
const generator_utils_1 = require("../util/generator-utils");
const langium_1 = require("langium");
const listener_generator_1 = require("./module/listener-generator");
const mapper_generator_1 = require("./module/mapper-generator");
const filter_generator_1 = require("./module/filter-generator");
const service_generator_1 = require("./module/service-generator");
const application_generator_1 = require("./module/application-generator");
const exception_generator_1 = require("./module/exception-generator");
const util_generator_1 = require("./module/util-generator");
function generateModules(model, target_folder) {
    var _a, _b, _c, _d, _e, _f;
    const package_path = (_b = (_a = model.configuration) === null || _a === void 0 ? void 0 : _a.package_path) !== null && _b !== void 0 ? _b : 'base';
    const modules = model.abstractElements.filter(ast_1.isModule);
    const ontology = (_d = (_c = model.configuration) === null || _c === void 0 ? void 0 : _c.database_name) !== null && _d !== void 0 ? _d : 'base';
    const topic_pattern = (_f = (_e = model.configuration) === null || _e === void 0 ? void 0 : _e.topic_pattern) !== null && _f !== void 0 ? _f : ``;
    if (model.configuration) {
        const package_name_application = `${package_path}.application`;
        const APPLICATION_PATH = (0, generator_utils_1.createPath)(target_folder, "src/main/java/", package_name_application.replaceAll(".", "/"));
        fs_1.default.writeFileSync(path_1.default.join(APPLICATION_PATH, `Application.java`), applicationGenerator(package_name_application, model.configuration));
    }
    const UTIL_PATH = (0, generator_utils_1.createPath)(target_folder, "src/main/java/", `${package_path}.util`.replaceAll(".", "/"));
    const util_package = `${package_path}.util`;
    fs_1.default.writeFileSync(path_1.default.join(UTIL_PATH, `Mapper.java`), mapperInterfaceGenerator(util_package));
    fs_1.default.writeFileSync(path_1.default.join(UTIL_PATH, `DateUtil.java`), (0, langium_1.toString)((0, util_generator_1.dataUtilGenerator)(util_package)));
    fs_1.default.writeFileSync(path_1.default.join(UTIL_PATH, `JsonUtil.java`), (0, langium_1.toString)((0, util_generator_1.jsonUtilGenerator)(util_package)));
    fs_1.default.writeFileSync(path_1.default.join(UTIL_PATH, `StringUtil.java`), (0, langium_1.toString)((0, util_generator_1.stringUtilGenerator)(util_package)));
    fs_1.default.writeFileSync(path_1.default.join(UTIL_PATH, `ApplicationAbstract.java`), (0, langium_1.toString)((0, application_generator_1.applicationAbstractGenerator)(util_package)));
    const UTIL_MONGO_PATH = (0, generator_utils_1.createPath)(target_folder, "src/main/java/", `${package_path}.util.mongo`.replaceAll(".", "/"));
    const util_mongo_package = `${package_path}.util.mongo`;
    fs_1.default.writeFileSync(path_1.default.join(UTIL_MONGO_PATH, `DataSearch.java`), (0, langium_1.toString)((0, util_generator_1.dataSearchGenerator)(util_mongo_package)));
    fs_1.default.writeFileSync(path_1.default.join(UTIL_MONGO_PATH, `MongoApplication.java`), (0, langium_1.toString)((0, application_generator_1.mongoApplicationGenerator)(util_mongo_package)));
    fs_1.default.writeFileSync(path_1.default.join(UTIL_MONGO_PATH, `MongoNotFound.java`), (0, langium_1.toString)((0, exception_generator_1.mongoExceptionGenerator)(util_mongo_package)));
    for (const mod of modules) {
        const package_name = `${package_path}.${mod.name.toLowerCase()}`;
        const MODULE_PATH = (0, generator_utils_1.createPath)(target_folder, "src/main/java/", package_name.replaceAll(".", "/"));
        const APPLICATIONS_PATH = (0, generator_utils_1.createPath)(MODULE_PATH, 'applications');
        const EXCEPTION_PATH = (0, generator_utils_1.createPath)(MODULE_PATH, 'exceptions');
        const MAPPER_PATH = (0, generator_utils_1.createPath)(MODULE_PATH, 'mappers');
        const SERVICE_PATH = (0, generator_utils_1.createPath)(MODULE_PATH, 'services');
        const LISTENER_PATH = (0, generator_utils_1.createPath)(MODULE_PATH, 'listeners');
        const FILTER_PATH = (0, generator_utils_1.createPath)(MODULE_PATH, 'filters');
        const mod_classes = mod.elements.filter(ast_1.isLocalEntity);
        for (const cls of mod_classes) {
            const class_name = cls.name;
            const MONGO_LISTENER_PATH = (0, generator_utils_1.createPath)(target_folder, "src/main/java/", `${package_name}.listeners.mongo`.replaceAll(".", "/"));
            fs_1.default.writeFileSync(path_1.default.join(EXCEPTION_PATH, `${class_name}ExceptionNotFound.java`), (0, langium_1.toString)((0, exception_generator_1.generateClassException)(cls, package_name)));
            fs_1.default.writeFileSync(path_1.default.join(APPLICATIONS_PATH, `${class_name}Application.java`), (0, langium_1.toString)((0, application_generator_1.generateClassApplication)(cls, package_name, util_package)));
            fs_1.default.writeFileSync(path_1.default.join(MONGO_LISTENER_PATH, `${class_name}InputListener.java`), (0, langium_1.toString)((0, listener_generator_1.generateListenerMongo)(cls, ontology, topic_pattern, package_name)));
            for (const functionMapping of cls.functions) {
                fs_1.default.writeFileSync(path_1.default.join(FILTER_PATH, `${functionMapping.name}Filter.java`), (0, langium_1.toString)((0, filter_generator_1.generateFilter)(cls, functionMapping, package_name)));
                fs_1.default.writeFileSync(path_1.default.join(MAPPER_PATH, `${functionMapping.name}Mapper.java`), (0, langium_1.toString)((0, mapper_generator_1.generateMapper)(cls, functionMapping, package_name, util_package)));
                fs_1.default.writeFileSync(path_1.default.join(LISTENER_PATH, `${functionMapping.name}Listener.java`), (0, langium_1.toString)((0, listener_generator_1.generateListener)(cls, topic_pattern, functionMapping, package_name)));
                fs_1.default.writeFileSync(path_1.default.join(SERVICE_PATH, `${cls.name}Service.java`), (0, langium_1.toString)((0, service_generator_1.generateService)(cls, functionMapping, package_name, util_package)));
            }
        }
    }
}
exports.generateModules = generateModules;
function mapperInterfaceGenerator(path_package) {
    return (0, langium_1.expandToStringWithNL) `
  package ${path_package};

  public interface Mapper <T> {
    public T map (String element) throws Exception;
  }
  `;
}
function applicationGenerator(path_package, configuration) {
    return (0, langium_1.expandToStringWithNL) `
  package ${path_package};

  import org.springframework.boot.SpringApplication;
  import org.springframework.boot.autoconfigure.SpringBootApplication;
  import org.springframework.boot.autoconfigure.domain.EntityScan;
  import org.springframework.context.annotation.ComponentScan;
  import org.springframework.kafka.annotation.EnableKafka;
  import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
  import org.springframework.transaction.annotation.EnableTransactionManagement;
  

  @EnableKafka  
  @SpringBootApplication  
  @ComponentScan(basePackages = {"${path_package.replace("application", "")}.*"})
  @EntityScan(basePackages = {"br.nemo.immigrant.ontology.entity.*"})
  @EnableJpaRepositories(basePackages = {"br.nemo.immigrant.ontology.entity.*"})
  @EnableTransactionManagement
  public class Application {

    public static void main(String[] args) {
      SpringApplication.run(Application.class, args);
    }
  }
  `;
}
//# sourceMappingURL=module-generator.js.map
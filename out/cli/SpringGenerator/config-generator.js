"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.generateConfigs = void 0;
const path_1 = __importDefault(require("path"));
const fs_1 = __importDefault(require("fs"));
const langium_1 = require("langium");
const generator_utils_1 = require("../util/generator-utils");
function generateConfigs(model, target_folder) {
    if (model.configuration) {
        fs_1.default.writeFileSync(path_1.default.join(target_folder, 'GUIDE.md'), (0, langium_1.toString)(generateGuide(model.configuration)));
        fs_1.default.writeFileSync(path_1.default.join(target_folder, 'docker-compose-database.yml'), (0, langium_1.toString)(generateComposeDatabase(model.configuration)));
        const RESOURCE_PATH = (0, generator_utils_1.createPath)(target_folder, "src/main/resources");
        fs_1.default.writeFileSync(path_1.default.join(target_folder, 'pom.xml'), (0, langium_1.toString)(generatePOMXML(model.configuration)));
        fs_1.default.writeFileSync(path_1.default.join(RESOURCE_PATH, 'logback.xml'), (0, langium_1.toString)(generatelogback()));
        fs_1.default.writeFileSync(path_1.default.join(RESOURCE_PATH, 'application.properties'), (0, langium_1.toString)(applicationProperties(model.configuration)));
    }
}
exports.generateConfigs = generateConfigs;
function generateGuide(configuration) {
    return (0, langium_1.expandToStringWithNL) `
  mvn  spring-boot:run
  `;
}
function applicationProperties(configuration) {
    return (0, langium_1.expandToStringWithNL) `

  spring.kafka.consumer.bootstrap-servers=${configuration.bootstrap_servers}
  spring.kafka.consumer.auto-offset-reset=earliest
  spring.kafka.consumer.key-deserializer=org.apache.kafka.common.serialization.StringDeserializer
  spring.kafka.consumer.value-deserializer=org.apache.kafka.common.serialization.StringDeserializer


  # Common Kafka Properties
  auto.create.topics.enable=true


  spring.datasource.initialization-mode=always
  spring.datasource.url = jdbc:postgresql://localhost:5432/${configuration.database_name}
  spring.datasource.username = postgres
  spring.datasource.password = postgres
  spring.datasource.platform= postgres
  logging.level.org.hibernate.SQL=DEBUG  

  spring.data.mongodb.host=localhost
  spring.data.mongodb.port=27017
  spring.data.mongodb.database=${configuration.database_name.toLowerCase()}
  `;
}
function generatelogback() {
    return (0, langium_1.expandToStringWithNL) `
  <?xml version="1.0" encoding="UTF-8"?>
  <configuration>
      <appender name="STDOUT" class="ch.qos.logback.core.ConsoleAppender">
          <encoder>
              <pattern>%d{HH:mm:ss.SSS} [%thread] %-5level %logger{36} - %msg%n
              </pattern>
          </encoder>
      </appender>

      <root level="INFO">
          <appender-ref ref="STDOUT" />
      </root>
  </configuration>
  `;
}
function generatePOMXML(configuration) {
    return (0, langium_1.expandToStringWithNL) `
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<parent>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-parent</artifactId>
		<version>3.1.0</version>
		<relativePath/> 
	</parent>
	<groupId>${configuration.package_path}</groupId>
	<artifactId>${configuration.software_name}</artifactId>
	<version>0.0.1-SNAPSHOT</version>
	<name>${configuration.software_name}</name>
	<description>${configuration.about}</description>
	<properties>
		<java.version>17</java.version>    
	</properties>
 	<dependencies>

   <dependency>
   <groupId>org.springframework.boot</groupId>
   <artifactId>spring-boot-starter-data-mongodb</artifactId>
 </dependency>

	 <dependency>
	 <groupId>org.springframework.boot</groupId>
	 <artifactId>spring-boot-starter-data-jpa</artifactId>
 </dependency>
 <dependency>
	 <groupId>org.postgresql</groupId>
	 <artifactId>postgresql</artifactId>
	 <scope>runtime</scope>
 </dependency>

 <dependency>
	 <groupId>org.springframework.data</groupId>
	 <artifactId>spring-data-commons</artifactId>
 </dependency>
 <dependency>
	 <groupId>org.apache.kafka</groupId>
	 <artifactId>kafka-streams</artifactId>
 </dependency>
 <dependency>
	 <groupId>org.springframework.kafka</groupId>
	 <artifactId>spring-kafka</artifactId>
 </dependency>

 <dependency>
	 <groupId>org.projectlombok</groupId>
	 <artifactId>lombok</artifactId>
 </dependency>


 <dependency>
	 <groupId>com.fasterxml.jackson.core</groupId>
	 <artifactId>jackson-databind</artifactId>
	 <version>2.13.0</version>
 </dependency>

 <dependency>
		  <groupId>jakarta.el</groupId>
		  <artifactId>jakarta.el-api</artifactId>
		  <version>4.0.0</version>
	  </dependency>


</dependencies>

<build>
 <plugins>
	 <plugin>
		 <groupId>org.springframework.boot</groupId>
		 <artifactId>spring-boot-maven-plugin</artifactId>
	 </plugin>
 </plugins>
</build>

</project>
  `;
}
function generateComposeDatabase(configuration) {
    var _a, _b;
    return (0, langium_1.expandToStringWithNL) `
    version: '3.7'

    services:
      postgres:
        image: postgres
        ports:
          - "5432:5432"
        restart: always
        environment:
          POSTGRES_PASSWORD: postgres
          POSTGRES_DB: ${(_b = (_a = configuration.database_name) !== null && _a !== void 0 ? _a : configuration.software_name) !== null && _b !== void 0 ? _b : 'KashmirDB'}
          POSTGRES_USER: postgres
        volumes:
          - ./data:/var/lib/postgresql
          - ./pg-initdb.d:/docker-entrypoint-initdb.d
  `;
}
//# sourceMappingURL=config-generator.js.map
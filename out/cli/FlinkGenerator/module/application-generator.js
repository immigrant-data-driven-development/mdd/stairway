"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.generateApplication = void 0;
const ast_1 = require("../../../language-server/generated/ast");
const langium_1 = require("langium");
function generateApplication(application) {
    var _a, _b, _c, _d, _e, _f;
    const result = (0, langium_1.expandToStringWithNL) `
  
  package ${(_a = application.configuration) === null || _a === void 0 ? void 0 : _a.package_path.replace('/', '.')}.application;

  import org.apache.flink.api.common.eventtime.WatermarkStrategy;
  import org.apache.flink.api.common.restartstrategy.RestartStrategies;
  import org.apache.flink.api.common.serialization.SimpleStringSchema;
  import org.apache.flink.api.common.time.Time;
  import org.apache.flink.api.java.tuple.Tuple2;
  import org.apache.flink.connector.kafka.source.KafkaSource;
  import org.apache.flink.connector.kafka.source.enumerator.initializer.OffsetsInitializer;
  import org.apache.flink.streaming.api.datastream.DataStream;
  import org.apache.flink.streaming.api.datastream.KeyedStream;
  import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
  import org.json.JSONObject;
  import java.util.concurrent.TimeUnit;
  import java.util.regex.Pattern;
  import ${(_c = (_b = application.configuration) === null || _b === void 0 ? void 0 : _b.package_path.toLocaleLowerCase()) !== null && _c !== void 0 ? _c : "".toLowerCase()}.mapper.JSONMapper;
  ${application.abstractElements.filter(ast_1.isModule).map(module => generateImport(module, application)).join("\n")}
  
  public class Application {

    final static String BOOTSTRAP_SERVERS = "${(_d = application.configuration) === null || _d === void 0 ? void 0 : _d.bootstrap_servers}";

    final static String TOPIC_PATTERN = "${(_e = application.configuration) === null || _e === void 0 ? void 0 : _e.topic_pattern}";

    final static KafkaSource<String> source = KafkaSource.<String>builder()
            .setBootstrapServers(BOOTSTRAP_SERVERS)
            .setTopicPattern(Pattern.compile(TOPIC_PATTERN))
            .setStartingOffsets(OffsetsInitializer.earliest())
            .setValueOnlyDeserializer(new SimpleStringSchema())
            .build();
    public static void main(String[] args) throws Exception {

        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(3);
      
        env.setRestartStrategy(RestartStrategies.fixedDelayRestart(
              3, // number of restart attempts
              Time.of(10, TimeUnit.SECONDS) // delay
        ));
      
        env.enableCheckpointing(1000);
      
        DataStream<Tuple2<String, JSONObject>> stream = env.fromSource(source, WatermarkStrategy.noWatermarks(), "Kafka Source").map(new JSONMapper()).name("Json Parser");
        
        KeyedStream<Tuple2<String, JSONObject>, String> elements = stream.keyBy(tuple -> tuple.f0);
        
        ${application.abstractElements.filter(ast_1.isModule).map(module => generateMapper(module)).join("\n")}
    
        env.execute("JOB ${(_f = application.configuration) === null || _f === void 0 ? void 0 : _f.software_name.toUpperCase()}");
    }
  }
  `;
    return result;
}
exports.generateApplication = generateApplication;
function generateMapper(module) {
    return (0, langium_1.expandToStringWithNL) `
  //Filtering and Mapping from module ${module.name}
  ${generateFilteringMapping(module)}
  ${generateConnect(module)}
  // Sink from ${module.name}   
  ${module.elements.filter(ast_1.isLocalEntity).map(entity => generateSink(entity)).join("\n")}
  `;
}
function generateConnect(module) {
    return (0, langium_1.expandToStringWithNL) `
${module.elements.filter(ast_1.isLocalEntity).map(entity => entity.functions.length > 1 ? "Connection" : "").join("\n")}
`;
}
// generate Sink
function generateSink(entity) {
    return (0, langium_1.expandToStringWithNL) `
  ${entity.name.toLowerCase()}DataStream.addSink(${entity.name}Sink.sink()).name("Saving ${entity.name}");
  `;
}
function generateFilteringMapping(module) {
    return (0, langium_1.expandToString) `
  ${module.elements.filter(ast_1.isLocalEntity).map(entity => entity.functions.map(f => `DataStream<${entity.name}> ${entity.functions.length > 1 ? f.name.toLowerCase() : entity.name.toLowerCase()}DataStream = elements.filter(key ->key.f0.equals("${f.source.toLowerCase()}"))${f.whenmappers.map(w => `.filter(new ${f.name}Filter())`)}.name("Selecting ${entity.name}").rebalance().map(new ${f.name}Mapper());`).join("\n")).join("\n")}
  `;
}
function generateImport(module, application) {
    var _a, _b;
    const package_path = (_b = (_a = application.configuration) === null || _a === void 0 ? void 0 : _a.package_path) !== null && _b !== void 0 ? _b : "";
    return (0, langium_1.expandToString) `
  ${module.elements.filter(ast_1.isLocalEntity).map(entity => entity.functions.map(f => `import ${package_path}.${module.name.toLowerCase()}.model.${entity.name};`)).join("\n")}
  ${module.elements.filter(ast_1.isLocalEntity).map(entity => entity.functions.map(f => `import ${package_path}.${module.name.toLowerCase()}.sink.${entity.name}Sink;`)).join("\n")}
  ${module.elements.filter(ast_1.isLocalEntity).map(entity => entity.functions.map(f => generateMapperImport(package_path, module.name.toLowerCase(), entity))).join('\n')}
  `;
}
function generateMapperImport(packagePath, moduleName, entity) {
    return (0, langium_1.expandToString) `
  ${entity.functions.map(mapper => `import ${packagePath}.${moduleName}.mapper.${mapper.name}Mapper;`).join("\n")}
  ${entity.functions.map(mapper => mapper.whenmappers.map(w => `import ${packagePath}.${moduleName}.mapper.${mapper.name}Filter;`)).join("\n")}
  `;
}
//# sourceMappingURL=application-generator.js.map
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.generateSink = void 0;
const langium_1 = require("langium");
const generator_utils_1 = require("../../generator-utils");
function generateSink(entity, configuration, sinkPath, modelPath) {
    var _a, _b, _c, _d, _e, _f;
    let count = 1;
    let attributes = [];
    let relations = [];
    if ((_b = (_a = entity.superType) === null || _a === void 0 ? void 0 : _a.ref) === null || _b === void 0 ? void 0 : _b.attributes) {
        attributes = (_d = (_c = entity.superType) === null || _c === void 0 ? void 0 : _c.ref) === null || _d === void 0 ? void 0 : _d.attributes;
        relations = (_f = (_e = entity.superType) === null || _e === void 0 ? void 0 : _e.ref) === null || _f === void 0 ? void 0 : _f.relations;
    }
    attributes = attributes.concat(entity.attributes);
    relations = relations.concat(entity.relations);
    const result = (0, langium_1.expandToStringWithNL) `

  package ${sinkPath.replaceAll('/', '.')};

  import org.apache.flink.connector.jdbc.JdbcConnectionOptions;
  import org.apache.flink.connector.jdbc.JdbcExecutionOptions;
  import org.apache.flink.connector.jdbc.JdbcSink;
  import org.apache.flink.streaming.api.functions.sink.SinkFunction;
  import java.sql.Date;
  import ${modelPath.replaceAll('/', '.')}.${entity.name};
  
  public class ${entity.name}Sink {
 
    public static  <T> SinkFunction<${entity.name}> sink(){

      final SinkFunction<${entity.name}> sink = JdbcSink.sink(
              "INSERT INTO ${entity.name.toLowerCase()} (${populateConstruct(attributes)}, created_at) VALUES (${questionMark(attributes)},?) ON CONFLICT (${uniqueAttributes(attributes)}) DO NOTHING;",
              (statement, ${entity.name.toLowerCase()}) -> {
                  ${attributes.map(attribute => `statement.set${(0, generator_utils_1.capitalizeString)(attribute.type)}(${count++}, ${entity.name.toLowerCase()}.${attribute.name.toLowerCase()});`).join("\n")}
                  statement.setDate(${count++},new Date(System.currentTimeMillis()));
              },
              JdbcExecutionOptions.builder()
                      .withBatchSize(1000)
                      .withBatchIntervalMs(200)
                      .withMaxRetries(5)
                      .build(),
              new JdbcConnectionOptions.JdbcConnectionOptionsBuilder()
                      .withUrl("jdbc:postgresql://localhost:5432/${configuration.database_name}")
                      .withDriverName("org.postgresql.Driver")
                      .withUsername("postgres")
                      .withPassword("postgres")
                      .build());
              return sink;
    }
  }
  `;
    return result;
}
exports.generateSink = generateSink;
function uniqueAttributes(attributes) {
    let attributesUnique = [];
    for (const at of attributes) {
        if (at === null || at === void 0 ? void 0 : at.unique) {
            attributesUnique.push(at);
        }
    }
    let result = (0, langium_1.expandToString) `
  ${attributesUnique.map(attribute => attribute.name.toLowerCase()).join(",")}
  `;
    return result.substring(0, result.length);
}
function questionMark(attributes) {
    let result = (0, langium_1.expandToString) `
  ${attributes.map(attribute => `?`).join(",")}
  `;
    return result.substring(0, result.length);
}
function populateConstruct(attributes) {
    let result = (0, langium_1.expandToString) `
  ${attributes.map(attribute => `${attribute.name.toLowerCase()}`).join(",")}
  `;
    return result.substring(0, result.length);
}
//# sourceMappingURL=sink-generator.js.map
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.generateModel = void 0;
const generator_utils_1 = require("../../generator-utils");
const langium_1 = require("langium");
function generateModel(entity, modelPath) {
    var _a, _b, _c, _d, _e, _f;
    let attributes = [];
    let relations = [];
    if ((_b = (_a = entity.superType) === null || _a === void 0 ? void 0 : _a.ref) === null || _b === void 0 ? void 0 : _b.attributes) {
        attributes = (_d = (_c = entity.superType) === null || _c === void 0 ? void 0 : _c.ref) === null || _d === void 0 ? void 0 : _d.attributes;
        relations = (_f = (_e = entity.superType) === null || _e === void 0 ? void 0 : _e.ref) === null || _f === void 0 ? void 0 : _f.relations;
    }
    attributes = attributes.concat(entity.attributes);
    relations = relations.concat(entity.relations);
    //${relations.map(relation => `public final Integer ${relation.name.toLowerCase()};`).join("\n")}
    return (0, langium_1.expandToString) `
    package ${modelPath.toLowerCase().replaceAll('/', '.')};
    import java.sql.Date;

    public class ${entity.name} {

        ${attributes.map(attribute => `public final ${(0, generator_utils_1.capitalizeString)(attribute.type)} ${attribute.name.toLowerCase()};`).join("\n")}
        public ${entity.name}(${createConstruct(attributes)}) {
            ${attributes.map(attribute => `this.${attribute.name.toLowerCase()} = ${attribute.name.toLowerCase()};`).join("\n")}
        }

        public String toString(){
            return ${createToString(attributes)}
        }
    }
    `;
}
exports.generateModel = generateModel;
function createConstruct(attributes) {
    let result = (0, langium_1.expandToString) `
    ${attributes.map(attribute => `${(0, generator_utils_1.capitalizeString)(attribute.type)} ${attribute.name.toLowerCase()}`).join(",")}
    `;
    return result.substring(0, result.length);
}
function createToString(attributes) {
    let result = (0, langium_1.expandToStringWithNL) `
    ${attributes.map(attribute => `"${(0, generator_utils_1.capitalizeString)(attribute.name)} :"+ this.${attribute.name.toLowerCase()}`).join("+\n")}
    `;
    result = result.substring(0, result.length - 1);
    return result + ";";
}
//# sourceMappingURL=model-generator.js.map
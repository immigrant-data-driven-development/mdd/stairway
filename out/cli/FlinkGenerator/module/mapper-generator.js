"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.generateMapper = void 0;
const generator_utils_1 = require("../../generator-utils");
const langium_1 = require("langium");
function generateMapper(entity, functionMapping, mapperPath, modelPath) {
    var _a, _b, _c, _d, _e, _f;
    let attributes = [];
    let relations = [];
    if ((_b = (_a = entity.superType) === null || _a === void 0 ? void 0 : _a.ref) === null || _b === void 0 ? void 0 : _b.attributes) {
        attributes = (_d = (_c = entity.superType) === null || _c === void 0 ? void 0 : _c.ref) === null || _d === void 0 ? void 0 : _d.attributes;
        relations = (_f = (_e = entity.superType) === null || _e === void 0 ? void 0 : _e.ref) === null || _f === void 0 ? void 0 : _f.relations;
    }
    attributes = attributes.concat(entity.attributes);
    relations = relations.concat(entity.relations);
    return (0, langium_1.expandToStringWithNL) `
  package ${mapperPath.replaceAll('/', '.')};
  
  import org.apache.flink.api.common.functions.MapFunction;
  import org.apache.flink.api.java.tuple.Tuple2;
  import org.json.JSONObject;
  import ${modelPath.replace('/', '.')}.${entity.name};

  public class ${functionMapping.name}Mapper implements MapFunction<Tuple2<String, JSONObject>, ${entity.name}> {

    public ${entity.name} map(Tuple2<String, JSONObject> stringJSONObjectTuple2) throws Exception {

      ${functionMapping.attributemappers.map(attributemapper => generateMap(attributemapper)).join("\n")}

      return new ${entity.name}(${populateConstruct(attributes)});
    }
  }
  `;
}
exports.generateMapper = generateMapper;
function generateMap(attributemapper) {
    var _a, _b, _c;
    return (0, langium_1.expandToStringWithNL) `
    ${(0, generator_utils_1.capitalizeString)((_b = (_a = attributemapper.target.ref) === null || _a === void 0 ? void 0 : _a.type) !== null && _b !== void 0 ? _b : "String")} ${(_c = attributemapper.target.ref) === null || _c === void 0 ? void 0 : _c.name.toLowerCase()} = stringJSONObjectTuple2.f1.getString("${attributemapper.source}");
  `;
}
function populateConstruct(attributes) {
    let result = (0, langium_1.expandToString) `
  ${attributes.map(attribute => `${attribute.name.toLowerCase()}`).join(",")}
  `;
    return result.substring(0, result.length);
}
//# sourceMappingURL=mapper-generator.js.map
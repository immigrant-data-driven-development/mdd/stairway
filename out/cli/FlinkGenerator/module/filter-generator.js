"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.generateFilter = void 0;
const langium_1 = require("langium");
function generateFilter(functionMapping, mapperPath, when) {
    return (0, langium_1.expandToStringWithNL) `
    package ${mapperPath.replaceAll('/', '.')};
    import org.apache.flink.api.common.functions.FilterFunction;
    import org.apache.flink.api.java.tuple.Tuple2;
    import org.json.JSONObject;

    public class ${functionMapping.name}Filter implements FilterFunction<Tuple2<String, JSONObject>> {
        @Override
        public boolean filter(Tuple2<String, JSONObject> stringJSONObjectTuple2) throws Exception {
            String element = stringJSONObjectTuple2.f1.getString("${when.source}");
            if (element.equals("${when.value}")) return true;
            return false;
        }
    }
    `;
}
exports.generateFilter = generateFilter;
//# sourceMappingURL=filter-generator.js.map
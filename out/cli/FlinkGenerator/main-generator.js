"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.generateMainFlink = void 0;
const fs_1 = __importDefault(require("fs"));
const helpers_generator_1 = require("./helpers-generator");
const module_generator_1 = require("./module-generator");
function generateMainFlink(application, target_folder) {
    fs_1.default.mkdirSync(target_folder, { recursive: true });
    (0, helpers_generator_1.generateHelpers)(application, target_folder);
    (0, module_generator_1.generateModule)(application, target_folder);
}
exports.generateMainFlink = generateMainFlink;
//# sourceMappingURL=main-generator.js.map
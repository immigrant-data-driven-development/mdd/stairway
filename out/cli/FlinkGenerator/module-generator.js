"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.generateModule = void 0;
const fs_1 = __importDefault(require("fs"));
const langium_1 = require("langium");
const path_1 = __importDefault(require("path"));
const ast_1 = require("../../language-server/generated/ast");
const generator_utils_1 = require("../generator-utils");
const application_generator_1 = require("./module/application-generator");
const filter_generator_1 = require("./module/filter-generator");
const mapper_generator_1 = require("./module/mapper-generator");
const model_generator_1 = require("./module/model-generator");
const sink_generator_1 = require("./module/sink-generator");
function generateModule(application, target_folder) {
    var _a, _b, _c;
    fs_1.default.mkdirSync(target_folder, { recursive: true });
    const package_path = (_b = (_a = application.configuration) === null || _a === void 0 ? void 0 : _a.package_path.replaceAll(".", "/")) !== null && _b !== void 0 ? _b : 'base';
    if (application.configuration) {
        for (const mod of application.abstractElements.filter(ast_1.isModule)) {
            const package_name = `${package_path}/${mod.name.toLowerCase()}/`;
            const MODULE_PATH = (0, generator_utils_1.createPath)(target_folder, "src/main/java/", package_name);
            const MODEL_PATH = (0, generator_utils_1.createPath)(MODULE_PATH, 'model');
            const MAPPER_PATH = (0, generator_utils_1.createPath)(MODULE_PATH, 'mapper');
            const SINK_PATH = (0, generator_utils_1.createPath)(MODULE_PATH, 'sink');
            const entities = mod.elements.filter(ast_1.isLocalEntity);
            for (const entity of entities) {
                const package_path_x = ((_c = application.configuration) === null || _c === void 0 ? void 0 : _c.package_path) + "." + mod.name.toLowerCase();
                if (entity.functions.length > 0) {
                    fs_1.default.writeFileSync(path_1.default.join(MODEL_PATH, `${entity.name}.java`), (0, model_generator_1.generateModel)(entity, package_path_x + ".model"));
                    fs_1.default.writeFileSync(path_1.default.join(SINK_PATH, `${entity.name}Sink.java`), (0, sink_generator_1.generateSink)(entity, application.configuration, package_path_x + ".sink", package_path_x + ".model"));
                }
                for (const functionMapping of entity.functions) {
                    fs_1.default.writeFileSync(path_1.default.join(MAPPER_PATH, `${functionMapping.name}Mapper.java`), (0, mapper_generator_1.generateMapper)(entity, functionMapping, package_path_x + ".mapper", package_path_x + ".model"));
                    for (const whenMapper of functionMapping.whenmappers) {
                        fs_1.default.writeFileSync(path_1.default.join(MAPPER_PATH, `${functionMapping.name}Filter.java`), (0, filter_generator_1.generateFilter)(functionMapping, package_path_x + ".mapper", whenMapper));
                    }
                }
            }
        }
        const JSONMAPPER_PATH = (0, generator_utils_1.createPath)(target_folder, "src/main/java/", package_path + "/mapper");
        fs_1.default.writeFileSync(path_1.default.join(JSONMAPPER_PATH, `JSONMapper.java`), generateMapperJSON(package_path + ".mapper"));
        const APPLICATION_PATH = (0, generator_utils_1.createPath)(target_folder, "src/main/java/", package_path + "/application");
        fs_1.default.writeFileSync(path_1.default.join(APPLICATION_PATH, `Application.java`), (0, application_generator_1.generateApplication)(application));
    }
}
exports.generateModule = generateModule;
function generateMapperJSON(package_name) {
    return (0, langium_1.expandToStringWithNL) `
    package ${package_name.replaceAll("/", ".")};
    import org.apache.flink.api.common.functions.MapFunction;
    import org.apache.flink.api.java.tuple.Tuple2;
    import org.json.JSONObject;

    public class JSONMapper implements MapFunction<String,Tuple2<String, JSONObject>> {

        @Override
        public Tuple2<String, JSONObject> map(String element) throws Exception {

            JSONObject jsonObject = new JSONObject(element);
            String entity = jsonObject.getString("entity");

            Tuple2<String, JSONObject> result = new Tuple2<>(entity, jsonObject);
            return result;
        }
    }
    `;
}
//# sourceMappingURL=module-generator.js.map
import { AstNode, AstNodeDescription, DefaultScopeComputation, LangiumDocument } from "langium";
import { CancellationToken } from "vscode-languageclient";
import { Application, isLocalEntity, isModule } from "./generated/ast";

/**
 * Gerador customizado para o escopo global do arquivo.
 * Por padrão, o escopo global só contém os filhos do nó raiz,
 * sejam acessíveis globalmente
 */
export class CustomScopeComputation extends DefaultScopeComputation {
    override async computeExports(document: LangiumDocument<AstNode>, cancelToken?: CancellationToken | undefined): Promise<AstNodeDescription[]> {
        // Os nós que normalmente estariam no escopo global
        const default_global = await super.computeExports(document, cancelToken)

        const root = document.parseResult.value as Application
        
        // Colocar no escopo global todas as LocalEntity de todos os Module com o nome normal
        const entities = root.abstractElements.filter(isModule).flatMap(m =>
            m.elements.filter(isLocalEntity).map(e =>
                this.descriptions.createDescription(e, `${e.$container.name}.${e.name}`, document)
            )
        )

        root.abstractElements.filter(isModule).flatMap(m => m.elements.filter(isLocalEntity).map(e =>
                this.exportNode(e, default_global, document)
            )
        )

        
        return default_global.concat(entities)
    }
}

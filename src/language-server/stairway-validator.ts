import { ValidationChecks } from 'langium';
import { StairwayAstType } from './generated/ast';
import type { StairwayServices } from './stairway-module';

/**
 * Register custom validation checks.
 */
export function registerValidationChecks(services: StairwayServices) {
    const registry = services.validation.ValidationRegistry;
    const validator = services.validation.StairwayValidator;
    const checks: ValidationChecks<StairwayAstType> = {
        
    };
    registry.register(checks, validator);
}

/**
 * Implementation of custom validations.
 */
export class StairwayValidator {

    

}

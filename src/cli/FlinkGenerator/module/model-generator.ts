import { Attribute, Entity, Relation } from "../../../language-server/generated/ast";
import {  capitalizeString } from "../../generator-utils";
import { expandToString, expandToStringWithNL } from "langium";


export function generateModel(entity : Entity, modelPath: String ): string {

    let attributes: Array<Attribute> = []
    let relations: Array<Relation> = []
    if (entity.superType?.ref?.attributes){
        attributes = entity.superType?.ref?.attributes
        relations = entity.superType?.ref?.relations
    }
    
    attributes = attributes.concat(entity.attributes)
    relations = relations.concat (entity.relations)
    //${relations.map(relation => `public final Integer ${relation.name.toLowerCase()};`).join("\n")}
    return expandToString`
    package ${modelPath.toLowerCase().replaceAll('/','.')};
    import java.sql.Date;

    public class ${entity.name} {

        ${attributes.map(attribute => `public final ${capitalizeString(attribute.type)} ${attribute.name.toLowerCase()};`).join("\n")}
        public ${entity.name}(${createConstruct(attributes)}) {
            ${attributes.map(attribute => `this.${attribute.name.toLowerCase()} = ${attribute.name.toLowerCase()};`).join("\n")}
        }

        public String toString(){
            return ${createToString(attributes)}
        }
    }
    `
}

function createConstruct(attributes: Array<Attribute>): string {
    let result = expandToString`
    ${attributes.map( attribute => `${capitalizeString(attribute.type)} ${attribute.name.toLowerCase()}`).join(",")}
    `
    
    return result.substring(0, result.length)
}

function createToString (attributes: Array<Attribute>): string {
    let result = expandToStringWithNL`
    ${attributes.map( attribute => `"${capitalizeString(attribute.name)} :"+ this.${attribute.name.toLowerCase()}`).join("+\n")}
    `
    result = result.substring(0, result.length -1)
    return result + ";"
}


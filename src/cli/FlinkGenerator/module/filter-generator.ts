import {WhenMapper,FunctionMapping } from "../../../language-server/generated/ast";
import { expandToStringWithNL } from "langium";

export function generateFilter(functionMapping : FunctionMapping, mapperPath: string, when: WhenMapper): string {

    return expandToStringWithNL`
    package ${mapperPath.replaceAll('/','.')};
    import org.apache.flink.api.common.functions.FilterFunction;
    import org.apache.flink.api.java.tuple.Tuple2;
    import org.json.JSONObject;

    public class ${functionMapping.name}Filter implements FilterFunction<Tuple2<String, JSONObject>> {
        @Override
        public boolean filter(Tuple2<String, JSONObject> stringJSONObjectTuple2) throws Exception {
            String element = stringJSONObjectTuple2.f1.getString("${when.source}");
            if (element.equals("${when.value}")) return true;
            return false;
        }
    }
    `
}


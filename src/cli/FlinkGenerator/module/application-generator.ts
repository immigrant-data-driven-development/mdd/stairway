import { Application, isLocalEntity, isModule, Module, Entity} from "../../../language-server/generated/ast";
import { expandToString, expandToStringWithNL } from "langium";

export function generateApplication( application: Application): string {
  
  const result: string  = expandToStringWithNL`
  
  package ${application.configuration?.package_path.replace('/','.')}.application;

  import org.apache.flink.api.common.eventtime.WatermarkStrategy;
  import org.apache.flink.api.common.restartstrategy.RestartStrategies;
  import org.apache.flink.api.common.serialization.SimpleStringSchema;
  import org.apache.flink.api.common.time.Time;
  import org.apache.flink.api.java.tuple.Tuple2;
  import org.apache.flink.connector.kafka.source.KafkaSource;
  import org.apache.flink.connector.kafka.source.enumerator.initializer.OffsetsInitializer;
  import org.apache.flink.streaming.api.datastream.DataStream;
  import org.apache.flink.streaming.api.datastream.KeyedStream;
  import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
  import org.json.JSONObject;
  import java.util.concurrent.TimeUnit;
  import java.util.regex.Pattern;
  import ${application.configuration?.package_path.toLocaleLowerCase() ?? "".toLowerCase()}.mapper.JSONMapper;
  ${application.abstractElements.filter(isModule).map(module =>generateImport(module,application)).join("\n")}
  
  public class Application {

    final static String BOOTSTRAP_SERVERS = "${application.configuration?.bootstrap_servers}";

    final static String TOPIC_PATTERN = "${application.configuration?.topic_pattern}";

    final static KafkaSource<String> source = KafkaSource.<String>builder()
            .setBootstrapServers(BOOTSTRAP_SERVERS)
            .setTopicPattern(Pattern.compile(TOPIC_PATTERN))
            .setStartingOffsets(OffsetsInitializer.earliest())
            .setValueOnlyDeserializer(new SimpleStringSchema())
            .build();
    public static void main(String[] args) throws Exception {

        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(3);
      
        env.setRestartStrategy(RestartStrategies.fixedDelayRestart(
              3, // number of restart attempts
              Time.of(10, TimeUnit.SECONDS) // delay
        ));
      
        env.enableCheckpointing(1000);
      
        DataStream<Tuple2<String, JSONObject>> stream = env.fromSource(source, WatermarkStrategy.noWatermarks(), "Kafka Source").map(new JSONMapper()).name("Json Parser");
        
        KeyedStream<Tuple2<String, JSONObject>, String> elements = stream.keyBy(tuple -> tuple.f0);
        
        ${application.abstractElements.filter(isModule).map(module =>generateMapper(module)).join("\n")}
    
        env.execute("JOB ${application.configuration?.software_name.toUpperCase()}");
    }
  }
  `
  return result;
}

function generateMapper(module: Module): string{

  return expandToStringWithNL`
  //Filtering and Mapping from module ${module.name}
  ${generateFilteringMapping(module)}
  ${generateConnect(module)}
  // Sink from ${module.name}   
  ${module.elements.filter(isLocalEntity).map(entity => generateSink(entity)).join("\n")}
  `
}

function generateConnect (module: Module): string{
return expandToStringWithNL`
${module.elements.filter(isLocalEntity).map(entity => entity.functions.length > 1? "Connection": "").join("\n")}
`
}
// generate Sink
function generateSink(entity: Entity):string {
  return expandToStringWithNL`
  ${entity.name.toLowerCase()}DataStream.addSink(${entity.name}Sink.sink()).name("Saving ${entity.name}");
  `
}


function generateFilteringMapping(module: Module):string {

 return expandToString`
  ${module.elements.filter(isLocalEntity).map(entity => entity.functions.map(f => `DataStream<${entity.name}> ${entity.functions.length >1 ? f.name.toLowerCase(): entity.name.toLowerCase()}DataStream = elements.filter(key ->key.f0.equals("${f.source.toLowerCase()}"))${f.whenmappers.map(w =>`.filter(new ${f.name}Filter())`)}.name("Selecting ${entity.name}").rebalance().map(new ${f.name}Mapper());`).join("\n")).join("\n")}
  `  
}

function generateImport(module: Module, application: Application): string {
  
  const package_path: string = application.configuration?.package_path ?? ""

  return expandToString`
  ${module.elements.filter(isLocalEntity).map(entity => entity.functions.map( f => `import ${package_path}.${module.name.toLowerCase()}.model.${entity.name};`)).join("\n")}
  ${module.elements.filter(isLocalEntity).map(entity => entity.functions.map (f => `import ${package_path}.${module.name.toLowerCase()}.sink.${entity.name}Sink;`)).join("\n")}
  ${module.elements.filter(isLocalEntity).map(entity => entity.functions.map( f=> generateMapperImport(package_path, module.name.toLowerCase(),entity))).join('\n')}
  `
}

function generateMapperImport(packagePath: string, moduleName: String, entity: Entity):string{

  return expandToString`
  ${entity.functions.map(mapper => `import ${packagePath}.${moduleName}.mapper.${mapper.name}Mapper;`).join("\n")}
  ${entity.functions.map(mapper => mapper.whenmappers.map(w=> `import ${packagePath}.${moduleName}.mapper.${mapper.name}Filter;`)).join("\n")}
  `
}
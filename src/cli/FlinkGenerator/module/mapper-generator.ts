import {Entity, FunctionMapping, Attribute, Relation, AttributesMapper } from "../../../language-server/generated/ast";
import { capitalizeString } from "../../generator-utils";
import { expandToStringWithNL, expandToString, } from "langium";

export function generateMapper(entity: Entity, functionMapping : FunctionMapping, mapperPath: string, modelPath: string ): string {
  
  let attributes: Array<Attribute> = []
  let relations: Array<Relation> = []


  if (entity.superType?.ref?.attributes){
    attributes = entity.superType?.ref?.attributes
    relations = entity.superType?.ref?.relations
  }
    
  attributes = attributes.concat(entity.attributes)
  relations = relations.concat (entity.relations)
  
  return expandToStringWithNL`
  package ${mapperPath.replaceAll('/','.')};
  
  import org.apache.flink.api.common.functions.MapFunction;
  import org.apache.flink.api.java.tuple.Tuple2;
  import org.json.JSONObject;
  import ${modelPath.replace('/','.')}.${entity.name};

  public class ${functionMapping.name}Mapper implements MapFunction<Tuple2<String, JSONObject>, ${entity.name}> {

    public ${entity.name} map(Tuple2<String, JSONObject> stringJSONObjectTuple2) throws Exception {

      ${functionMapping.attributemappers.map(attributemapper => generateMap(attributemapper)).join("\n")}

      return new ${entity.name}(${populateConstruct(attributes)});
    }
  }
  `

}  

function generateMap(attributemapper: AttributesMapper):string{
  
  return expandToStringWithNL`
    ${capitalizeString(attributemapper.target.ref?.type ?? "String")} ${attributemapper.target.ref?.name.toLowerCase()} = stringJSONObjectTuple2.f1.getString("${attributemapper.source}");
  `
}

function populateConstruct(attributes: Array<Attribute>): string {
  let result = expandToString`
  ${attributes.map( attribute => `${attribute.name.toLowerCase()}`).join(",")}
  `
  return result.substring(0, result.length)
}  
  

  

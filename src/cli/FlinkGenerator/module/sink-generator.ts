import { Entity, Configuration, Attribute, Relation } from "../../../language-server/generated/ast";
import { expandToString, expandToStringWithNL } from "langium";
import { capitalizeString } from "../../generator-utils"

export function generateSink(entity : Entity, configuration: Configuration, sinkPath: string, modelPath: string ): string {
  
  let count: number = 1; 
  let attributes: Array<Attribute> = []
  let relations: Array<Relation> = []
  if (entity.superType?.ref?.attributes){
        attributes = entity.superType?.ref?.attributes
        relations = entity.superType?.ref?.relations
  }
    
  attributes = attributes.concat(entity.attributes)
  relations = relations.concat (entity.relations)
  
  const result: string  = expandToStringWithNL`

  package ${sinkPath.replaceAll('/','.')};

  import org.apache.flink.connector.jdbc.JdbcConnectionOptions;
  import org.apache.flink.connector.jdbc.JdbcExecutionOptions;
  import org.apache.flink.connector.jdbc.JdbcSink;
  import org.apache.flink.streaming.api.functions.sink.SinkFunction;
  import java.sql.Date;
  import ${modelPath.replaceAll('/','.')}.${entity.name};
  
  public class ${entity.name}Sink {
 
    public static  <T> SinkFunction<${entity.name}> sink(){

      final SinkFunction<${entity.name}> sink = JdbcSink.sink(
              "INSERT INTO ${entity.name.toLowerCase()} (${populateConstruct(attributes)}, created_at) VALUES (${questionMark(attributes)},?) ON CONFLICT (${uniqueAttributes(attributes)}) DO NOTHING;",
              (statement, ${entity.name.toLowerCase()}) -> {
                  ${attributes.map(attribute => `statement.set${capitalizeString(attribute.type)}(${count++}, ${entity.name.toLowerCase()}.${attribute.name.toLowerCase()});`).join("\n")}
                  statement.setDate(${count++},new Date(System.currentTimeMillis()));
              },
              JdbcExecutionOptions.builder()
                      .withBatchSize(1000)
                      .withBatchIntervalMs(200)
                      .withMaxRetries(5)
                      .build(),
              new JdbcConnectionOptions.JdbcConnectionOptionsBuilder()
                      .withUrl("jdbc:postgresql://localhost:5432/${configuration.database_name}")
                      .withDriverName("org.postgresql.Driver")
                      .withUsername("postgres")
                      .withPassword("postgres")
                      .build());
              return sink;
    }
  }
  `
  return result;
}

function uniqueAttributes(attributes: Array<Attribute>):string {
  let attributesUnique: Array<Attribute> = []

  for (const at of attributes ){
    if (at?.unique){
      attributesUnique.push(at)
    }
  }

  let result = expandToString`
  ${attributesUnique.map(attribute => attribute.name.toLowerCase()).join(",")}
  `
  return result.substring(0, result.length)
}

function questionMark(attributes: Array<Attribute>):string {
  let result = expandToString`
  ${attributes.map( attribute => `?`).join(",")}
  `
  return result.substring(0, result.length)
}

function populateConstruct(attributes: Array<Attribute>): string {
  let result = expandToString`
  ${attributes.map( attribute => `${attribute.name.toLowerCase()}`).join(",")}
  `
  return result.substring(0, result.length)
}
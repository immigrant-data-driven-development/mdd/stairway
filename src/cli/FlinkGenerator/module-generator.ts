import fs from "fs";
import { expandToStringWithNL } from "langium";

import path from "path";
import { Application,isModule,isLocalEntity} from "../../language-server/generated/ast";
import { createPath } from "../generator-utils";
import { generateApplication } from "./module/application-generator";
import { generateFilter } from "./module/filter-generator";
import { generateMapper } from "./module/mapper-generator";
import { generateModel } from "./module/model-generator";
import { generateSink } from "./module/sink-generator";

export function generateModule(application: Application, target_folder: string): void {
    
    fs.mkdirSync(target_folder, {recursive:true})
    const package_path  = application.configuration?.package_path.replaceAll(".","/") ?? 'base'
    
    if (application.configuration)
    {
        for(const mod of application.abstractElements.filter(isModule)) {
    
            const package_name      = `${package_path}/${mod.name.toLowerCase()}/`
            
            const MODULE_PATH       = createPath(target_folder, "src/main/java/", package_name)
            const MODEL_PATH        = createPath(MODULE_PATH, 'model')
            const MAPPER_PATH       = createPath(MODULE_PATH, 'mapper')
            const SINK_PATH         = createPath(MODULE_PATH, 'sink')
        
            const entities = mod.elements.filter(isLocalEntity)
            
            for (const entity of entities)
            {
                const package_path_x = application.configuration?.package_path+"."+mod.name.toLowerCase()
                if (entity.functions.length > 0){
                    
                    fs.writeFileSync(path.join(MODEL_PATH, `${entity.name}.java`), generateModel(entity,package_path_x+".model"))
                    fs.writeFileSync(path.join(SINK_PATH, `${entity.name}Sink.java`), generateSink(entity,application.configuration, package_path_x+".sink", package_path_x+".model"))    
                }
                
                for (const functionMapping of entity.functions) {
                    fs.writeFileSync(path.join(MAPPER_PATH, `${functionMapping.name}Mapper.java`), generateMapper(entity, functionMapping, package_path_x+".mapper", package_path_x+".model"))
                    for (const whenMapper of functionMapping.whenmappers){
                        fs.writeFileSync(path.join(MAPPER_PATH, `${functionMapping.name}Filter.java`), generateFilter(functionMapping, package_path_x+".mapper",whenMapper))
                    }
                }
                
            }
        }

        const JSONMAPPER_PATH = createPath(target_folder, "src/main/java/", package_path+"/mapper")
        fs.writeFileSync(path.join(JSONMAPPER_PATH, `JSONMapper.java`), generateMapperJSON(package_path+".mapper"))
        
        const APPLICATION_PATH = createPath(target_folder, "src/main/java/", package_path+"/application")
        fs.writeFileSync(path.join(APPLICATION_PATH, `Application.java`), generateApplication(application))
       
    }
}

function generateMapperJSON(package_name: string):string {
    return expandToStringWithNL`
    package ${package_name.replaceAll("/", ".")};
    import org.apache.flink.api.common.functions.MapFunction;
    import org.apache.flink.api.java.tuple.Tuple2;
    import org.json.JSONObject;

    public class JSONMapper implements MapFunction<String,Tuple2<String, JSONObject>> {

        @Override
        public Tuple2<String, JSONObject> map(String element) throws Exception {

            JSONObject jsonObject = new JSONObject(element);
            String entity = jsonObject.getString("entity");

            Tuple2<String, JSONObject> result = new Tuple2<>(entity, jsonObject);
            return result;
        }
    }
    `
}


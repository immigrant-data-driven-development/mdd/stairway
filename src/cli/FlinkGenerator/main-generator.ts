import fs from "fs";
import { Application } from "../../language-server/generated/ast";
import { generateHelpers } from "./helpers-generator";
import {generateModule} from "./module-generator";

export function generateMainFlink(application: Application, target_folder: string) : void {
  fs.mkdirSync(target_folder, {recursive:true})
  generateHelpers(application, target_folder)
  generateModule(application, target_folder)
}
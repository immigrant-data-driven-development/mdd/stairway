import path from 'path'
import fs from 'fs'

import { Application,Configuration } from "../../language-server/generated/ast";
import { expandToStringWithNL, Generated, toString } from 'langium';
import { createPath } from '../util/generator-utils';

export function generateConfigs(model: Application, target_folder: string) {
  
 
  if (model.configuration){
    
    fs.writeFileSync(path.join(target_folder, 'GUIDE.md'), toString(generateGuide(model.configuration)))
    fs.writeFileSync(path.join(target_folder, 'docker-compose-database.yml'), toString(generateComposeDatabase(model.configuration)))
    
    const RESOURCE_PATH = createPath(target_folder, "src/main/resources")
    fs.writeFileSync(path.join(target_folder, 'pom.xml'), toString(generatePOMXML(model.configuration)))
    fs.writeFileSync(path.join(RESOURCE_PATH, 'logback.xml'), toString(generatelogback()))
    fs.writeFileSync(path.join(RESOURCE_PATH, 'application.properties'), toString(applicationProperties(model.configuration)))


  }
  
}


function generateGuide(configuration: Configuration): Generated{
  return expandToStringWithNL`
  mvn  spring-boot:run
  `
}

function applicationProperties(configuration: Configuration):Generated{
  return expandToStringWithNL`

  spring.kafka.consumer.bootstrap-servers=${configuration.bootstrap_servers}
  spring.kafka.consumer.auto-offset-reset=earliest
  spring.kafka.consumer.key-deserializer=org.apache.kafka.common.serialization.StringDeserializer
  spring.kafka.consumer.value-deserializer=org.apache.kafka.common.serialization.StringDeserializer


  # Common Kafka Properties
  auto.create.topics.enable=true


  spring.datasource.initialization-mode=always
  spring.datasource.url = jdbc:postgresql://localhost:5432/${configuration.database_name}
  spring.datasource.username = postgres
  spring.datasource.password = postgres
  spring.datasource.platform= postgres
  logging.level.org.hibernate.SQL=DEBUG  

  spring.data.mongodb.host=localhost
  spring.data.mongodb.port=27017
  spring.data.mongodb.database=${configuration.database_name.toLowerCase()}
  `
}
function generatelogback(): Generated{
  return expandToStringWithNL`
  <?xml version="1.0" encoding="UTF-8"?>
  <configuration>
      <appender name="STDOUT" class="ch.qos.logback.core.ConsoleAppender">
          <encoder>
              <pattern>%d{HH:mm:ss.SSS} [%thread] %-5level %logger{36} - %msg%n
              </pattern>
          </encoder>
      </appender>

      <root level="INFO">
          <appender-ref ref="STDOUT" />
      </root>
  </configuration>
  `
}

function generatePOMXML(configuration: Configuration) : Generated {
  
  
  return expandToStringWithNL`
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<parent>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-parent</artifactId>
		<version>3.1.0</version>
		<relativePath/> 
	</parent>
	<groupId>${configuration.package_path}</groupId>
	<artifactId>${configuration.software_name}</artifactId>
	<version>0.0.1-SNAPSHOT</version>
	<name>${configuration.software_name}</name>
	<description>${configuration.about}</description>
	<properties>
		<java.version>17</java.version>    
	</properties>
 	<dependencies>

   <dependency>
   <groupId>org.springframework.boot</groupId>
   <artifactId>spring-boot-starter-data-mongodb</artifactId>
 </dependency>

	 <dependency>
	 <groupId>org.springframework.boot</groupId>
	 <artifactId>spring-boot-starter-data-jpa</artifactId>
 </dependency>
 <dependency>
	 <groupId>org.postgresql</groupId>
	 <artifactId>postgresql</artifactId>
	 <scope>runtime</scope>
 </dependency>

 <dependency>
	 <groupId>org.springframework.data</groupId>
	 <artifactId>spring-data-commons</artifactId>
 </dependency>
 <dependency>
	 <groupId>org.apache.kafka</groupId>
	 <artifactId>kafka-streams</artifactId>
 </dependency>
 <dependency>
	 <groupId>org.springframework.kafka</groupId>
	 <artifactId>spring-kafka</artifactId>
 </dependency>

 <dependency>
	 <groupId>org.projectlombok</groupId>
	 <artifactId>lombok</artifactId>
 </dependency>


 <dependency>
	 <groupId>com.fasterxml.jackson.core</groupId>
	 <artifactId>jackson-databind</artifactId>
	 <version>2.13.0</version>
 </dependency>

 <dependency>
		  <groupId>jakarta.el</groupId>
		  <artifactId>jakarta.el-api</artifactId>
		  <version>4.0.0</version>
	  </dependency>


</dependencies>

<build>
 <plugins>
	 <plugin>
		 <groupId>org.springframework.boot</groupId>
		 <artifactId>spring-boot-maven-plugin</artifactId>
	 </plugin>
 </plugins>
</build>

</project>
  `
}

function generateComposeDatabase(configuration: Configuration) : Generated {
  return expandToStringWithNL`
    version: '3.7'

    services:
      postgres:
        image: postgres
        ports:
          - "5432:5432"
        restart: always
        environment:
          POSTGRES_PASSWORD: postgres
          POSTGRES_DB: ${configuration.database_name ?? configuration.software_name ?? 'KashmirDB'}
          POSTGRES_USER: postgres
        volumes:
          - ./data:/var/lib/postgresql
          - ./pg-initdb.d:/docker-entrypoint-initdb.d
  `
}


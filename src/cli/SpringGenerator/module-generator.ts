import path from "path";
import fs from "fs";

import { Application, isLocalEntity, isModule, Configuration } from "../../language-server/generated/ast";
import { createPath } from "../util/generator-utils";
import { expandToStringWithNL, toString } from "langium";

import { generateListener, generateListenerMongo} from "./module/listener-generator";
import { generateMapper} from "./module/mapper-generator";
import { generateFilter} from "./module/filter-generator";
import { generateService } from "./module/service-generator";
import {applicationAbstractGenerator, mongoApplicationGenerator, generateClassApplication } from "./module/application-generator";
import {generateClassException, mongoExceptionGenerator} from "./module/exception-generator";
import {dataUtilGenerator,jsonUtilGenerator, stringUtilGenerator, dataSearchGenerator} from "./module/util-generator";

export function generateModules(model: Application, target_folder: string) : void {
  
  const package_path  = model.configuration?.package_path ?? 'base'

  const modules =  model.abstractElements.filter(isModule);

  const ontology = model.configuration?.database_name ?? 'base'

  const topic_pattern = model.configuration?.topic_pattern ?? ``

  if (model.configuration){
    
    const package_name_application      = `${package_path}.application`
    const APPLICATION_PATH       = createPath(target_folder, "src/main/java/", package_name_application.replaceAll(".","/"))
    
    fs.writeFileSync(path.join(APPLICATION_PATH,`Application.java`), applicationGenerator(package_name_application, model.configuration))
    
  }

  const UTIL_PATH  = createPath(target_folder, "src/main/java/", `${package_path}.util`.replaceAll(".","/"))
  const util_package= `${package_path}.util`
  
  fs.writeFileSync(path.join(UTIL_PATH,`Mapper.java`), mapperInterfaceGenerator(util_package))
  fs.writeFileSync(path.join(UTIL_PATH,`DateUtil.java`), toString(dataUtilGenerator(util_package)))
  fs.writeFileSync(path.join(UTIL_PATH,`JsonUtil.java`), toString(jsonUtilGenerator(util_package)))
  fs.writeFileSync(path.join(UTIL_PATH,`StringUtil.java`), toString(stringUtilGenerator(util_package)))
  fs.writeFileSync(path.join(UTIL_PATH,`ApplicationAbstract.java`), toString(applicationAbstractGenerator(util_package)))

  const UTIL_MONGO_PATH  = createPath(target_folder, "src/main/java/", `${package_path}.util.mongo`.replaceAll(".","/"))
  const util_mongo_package= `${package_path}.util.mongo`

  fs.writeFileSync(path.join(UTIL_MONGO_PATH,`DataSearch.java`), toString(dataSearchGenerator(util_mongo_package)))
  fs.writeFileSync(path.join(UTIL_MONGO_PATH,`MongoApplication.java`), toString(mongoApplicationGenerator(util_mongo_package)))
  fs.writeFileSync(path.join(UTIL_MONGO_PATH,`MongoNotFound.java`), toString(mongoExceptionGenerator(util_mongo_package)))
  

  for(const mod of modules) {
    
    const package_name          = `${package_path}.${mod.name.toLowerCase()}`
    const MODULE_PATH           = createPath(target_folder, "src/main/java/", package_name.replaceAll(".","/"))
    const APPLICATIONS_PATH     = createPath(MODULE_PATH, 'applications')    
    const EXCEPTION_PATH        = createPath(MODULE_PATH, 'exceptions')
    const MAPPER_PATH           = createPath(MODULE_PATH, 'mappers')
    const SERVICE_PATH          = createPath(MODULE_PATH, 'services')
    const LISTENER_PATH         = createPath(MODULE_PATH, 'listeners')
    const FILTER_PATH           = createPath(MODULE_PATH, 'filters')
    

    
    const mod_classes = mod.elements.filter(isLocalEntity)

    for(const cls of mod_classes) {
      
      const class_name = cls.name
      
      const MONGO_LISTENER_PATH  = createPath(target_folder, "src/main/java/", `${package_name}.listeners.mongo`.replaceAll(".","/"))
         
      fs.writeFileSync(path.join(EXCEPTION_PATH,    `${class_name}ExceptionNotFound.java`), toString(generateClassException(cls, package_name)))
      fs.writeFileSync(path.join(APPLICATIONS_PATH, `${class_name}Application.java`), toString(generateClassApplication(cls, package_name,util_package)))
      fs.writeFileSync(path.join(MONGO_LISTENER_PATH, `${class_name}InputListener.java`), toString(generateListenerMongo(cls,ontology, topic_pattern, package_name)))
      
      for (const functionMapping of cls.functions){
        
        fs.writeFileSync(path.join(FILTER_PATH, `${functionMapping.name}Filter.java`), toString(generateFilter(cls, functionMapping, package_name)))  
        
        fs.writeFileSync(path.join(MAPPER_PATH, `${functionMapping.name}Mapper.java`), toString(generateMapper(cls, functionMapping, package_name, util_package)))
        
        fs.writeFileSync(path.join(LISTENER_PATH, `${functionMapping.name}Listener.java`), toString(generateListener(cls, topic_pattern, functionMapping, package_name)))

        fs.writeFileSync(path.join(SERVICE_PATH, `${cls.name}Service.java`), toString(generateService(cls, functionMapping, package_name,util_package)))
        
      }      
      
    }    
  }
}





function mapperInterfaceGenerator (path_package: string):string {
  return expandToStringWithNL`
  package ${path_package};

  public interface Mapper <T> {
    public T map (String element) throws Exception;
  }
  `
}

function applicationGenerator(path_package: string, configuration: Configuration):string{
  return expandToStringWithNL`
  package ${path_package};

  import org.springframework.boot.SpringApplication;
  import org.springframework.boot.autoconfigure.SpringBootApplication;
  import org.springframework.boot.autoconfigure.domain.EntityScan;
  import org.springframework.context.annotation.ComponentScan;
  import org.springframework.kafka.annotation.EnableKafka;
  import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
  import org.springframework.transaction.annotation.EnableTransactionManagement;
  

  @EnableKafka  
  @SpringBootApplication  
  @ComponentScan(basePackages = {"${path_package.replace("application", "")}.*"})
  @EntityScan(basePackages = {"br.nemo.immigrant.ontology.entity.*"})
  @EnableJpaRepositories(basePackages = {"br.nemo.immigrant.ontology.entity.*"})
  @EnableTransactionManagement
  public class Application {

    public static void main(String[] args) {
      SpringApplication.run(Application.class, args);
    }
  }
  `
  
}

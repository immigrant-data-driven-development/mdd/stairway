
import {LocalEntity, Relation } from "../../../language-server/generated/ast";
import { expandToStringWithNL, Generated } from "langium";


function generateConditionsCreateFunctionApplication (relation: Relation): Generated{
    return expandToStringWithNL`
    if (${relation.type.ref?.name.toLocaleLowerCase()}DataSearch.getElementValue() !=null && ${relation.type.ref?.name.toLocaleLowerCase()}DataSearch.getElementValue() != 0){
      Document document = this.retrieveDocument(${relation.type.ref?.name.toLocaleLowerCase()}DataSearch);
      String internalID = JsonUtil.retrieveInternalID(document.toJson());
      ${relation.type.ref?.name} ${relation.type.ref?.name.toLocaleLowerCase()} = this.${relation.type.ref?.name.toLocaleLowerCase()}Application.retrieveByInternalID(internalID);
      instance.set${relation.type.ref?.name.toLocaleLowerCase()}(${relation.type.ref?.name.toLocaleLowerCase()});
  
    }
    `
  }

function generateCreateFunctionApplication (cls: LocalEntity): Generated{
  
    return expandToStringWithNL`
     
    public  ${cls.name} create( ${cls.name} instance, String payload) throws Exception, MongoNotFound ${cls.relations.map(relation =>`${relation.type.ref?.name}ExceptionNotFound`).join(`,`)}{
      ${cls.relations.map(relation =>`DataSearch ${relation.type.ref?.name.toLocaleLowerCase()}DataSearch = JsonUtil.retrieve(payload,"${relation.type.ref?.name.toLocaleLowerCase()}","${relation.type.ref?.name.toLocaleLowerCase()}_id");`).join(`\n`)}
  
      ${cls.relations.map(relation => generateConditionsCreateFunctionApplication(relation)).join("\n")}
      
      return this.create(instance);
    }
    `
  }

function generateApplicationtoApplication(relation: Relation): Generated {
    return expandToStringWithNL`
    @Autowired
    private ${relation.type.ref?.name}Application ${relation.type.ref?.name.toLocaleLowerCase()}Application;
    `
  }
  
export function generateClassApplication(localEntity: LocalEntity, package_name: string, package_util:string) : Generated {
    return expandToStringWithNL`
      package ${package_name}.applications;
      
      import ${localEntity.ontology?.module}.${localEntity.name};
      import ${localEntity.ontology?.module.replace('models','repositories')}.${localEntity.name}Repository;
      ${localEntity.relations.map(relation => `import <todo>.exceptions.${relation.type.ref?.name}ExceptionNotFound`).join(';\n')}
      import ${package_util}.ApplicationAbstract;
      import ${package_util}.mongo.MongoNotFound;
      import ${package_name}.exceptions.${localEntity.name}ExceptionNotFound;
      import org.springframework.stereotype.Component;
      import org.springframework.transaction.annotation.Transactional;
      import java.util.Optional;
      import org.springframework.beans.factory.annotation.Autowired;
      import org.bson.Document;
      import br.nemo.immigrant.ontology.entity.eo.teams.repositories.projections.IDProjection;
  
      @Component
      @Transactional
      public class ${localEntity.name}Application extends ApplicationAbstract  {   
      
      @Autowired
      private ${localEntity.name}Repository repository;
  
      ${localEntity.relations.map(relation => generateApplicationtoApplication(relation)).join("\n")}
  
  
      public ${localEntity.name} create(${localEntity.name} instance) {
        return this.repository.save(instance);
      }
  
      ${generateCreateFunctionApplication(localEntity)} 
  
  
      public ${localEntity.name} retrieveByExternalID(String externalID) throws ${localEntity.name}ExceptionNotFound {
          Optional<IDProjection> result = this.repository.findByExternalId(externalID);
  
          IDProjection projection = result.orElseThrow(() -> new ${localEntity.name}ExceptionNotFound(externalID));
  
          return createInstance(projection);
      }
  
      public ${localEntity.name} retrieveByInternalID(String internalID) throws ${localEntity.name}ExceptionNotFound {
          Optional<IDProjection> result = this.repository.findByInternalId(internalID);
  
          IDProjection projection = result.orElseThrow(() -> new ${localEntity.name}ExceptionNotFound(internalID));
  
          return createInstance(projection);
      }
  
      private ${localEntity.name} createInstance(IDProjection projection){
          return  ${localEntity.name}.builder().id(projection.getId()).externalId(projection.getExternalId()).internalId(projection.getInternalId()).name(projection.getName()).build();
      }
  
      public Boolean exists (String internalID){
          return this.repository.existsByInternalId(internalID);
      }
  
    }
    `
    
  
  }

export function mongoApplicationGenerator (path_package: string):string {
    return expandToStringWithNL`
    package ${path_package};
  
    import lombok.extern.slf4j.Slf4j;
    import org.springframework.stereotype.Component;
    import org.springframework.transaction.annotation.Transactional;
    import org.bson.Document;
    import org.springframework.data.mongodb.core.query.BasicQuery;
    import org.springframework.beans.factory.annotation.Autowired;
    import org.springframework.data.mongodb.core.MongoTemplate;
    import java.util.List;
  
    @Component
    @Transactional
    @Slf4j
    public class MongoApplication {
    
        @Autowired
        private MongoTemplate mongoTemplate;
    
        public static final String COLLECTION = "input";
        public List<Document>  find(String tableName, Long id, String database) throws MongoNotFound{
    
            String jsonQuery = "{$and:[{\\"payload.source.table\\":\\"{TABLE}\\"},{\\"payload.after.id\\":{ID}},{\\"payload.source.db\\":\\"{DATABASE}\\"}]}";
    
            jsonQuery = jsonQuery.replace("{TABLE}", tableName);
            jsonQuery = jsonQuery.replace("{ID}", String.valueOf(id));
            jsonQuery = jsonQuery.replace("{DATABASE}", database);
    
            log.info(" jsonQuery: {}", jsonQuery);
    
            BasicQuery query = new BasicQuery(jsonQuery);
            List<Document> documents = mongoTemplate.find(query, Document.class, COLLECTION);
    
            if (documents.isEmpty()) throw new MongoNotFound(tableName+"-"+id+"-"+database);
    
            return documents;
        }
    }
    `
  } 
  
export function applicationAbstractGenerator( package_name: string) : Generated {
    return expandToStringWithNL`
    package ${package_name};
  
    import ${package_name}.mongo.MongoApplication;
    import ${package_name}.mongo.MongoNotFound;
    import ${package_name}.mongo.DataSearch;
    import org.springframework.beans.factory.annotation.Autowired;
    import org.bson.Document;
    import java.util.List;
  
    public abstract class ApplicationAbstract {
  
        @Autowired
        private MongoApplication mongoApplication;
  
        protected Document retrieveDocument(DataSearch data) throws MongoNotFound {
            List<Document> documents = mongoApplication.find(data.getTable(),
                    data.getElementValue(),
                    data.getDatabase());
            return documents.get(0);
        }
    }
    `
  }
  

import {LocalEntity } from "../../../language-server/generated/ast";
import { expandToStringWithNL, Generated } from "langium";

export function generateClassException (cls: LocalEntity, package_name: string) : Generated {
    return expandToStringWithNL`
    package ${package_name}.exceptions;
  
    public class ${cls.name}ExceptionNotFound extends RuntimeException{
  
      public ${cls.name}ExceptionNotFound(String message) {
          super(message);
      }
  }
  
    `
  }
export function mongoExceptionGenerator (path_package: string):string {
    return expandToStringWithNL`
    package ${path_package};
  
    public class MongoNotFound extends RuntimeException{
  
      public MongoNotFound(String message) {
          super(message);
      }
    }
    `
  } 
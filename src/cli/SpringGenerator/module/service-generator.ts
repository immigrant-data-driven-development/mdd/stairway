import {expandToStringWithNL, Generated } from "langium";
import {LocalEntity, FunctionMapping } from "../../../language-server/generated/ast";

export function generateService(localEntity: LocalEntity, functionMapping: FunctionMapping, package_name: string, util_package: string): Generated {
    
    return expandToStringWithNL`

    package ${package_name}.services;
    import ${localEntity.ontology?.module}.${localEntity.name};
    ${localEntity.relations.map(relation => `import <todo>.exceptions.${relation.type.ref?.name}ExceptionNotFound`).join(';\n')}
    import ${package_name}.applications.${localEntity.name}Application;
    import ${package_name}.mappers.${functionMapping.name}Mapper;
    import ${util_package}.Mapper;

    import lombok.RequiredArgsConstructor;
    import lombok.extern.slf4j.Slf4j;
    import org.apache.kafka.clients.consumer.ConsumerRecord;
    import org.springframework.beans.factory.annotation.Autowired;
    import org.springframework.beans.factory.annotation.Value;
    import org.springframework.stereotype.Component;

    @Slf4j
    @RequiredArgsConstructor
    @Component
    public class ${localEntity.name}Service {

        
        @Autowired
        private ${localEntity.name}Application application;        

        public void process(ConsumerRecord<String, String> payload,Mapper<${localEntity.name}> mapper) throws ${localEntity.relations.map(relation => `${relation.type.ref?.name}ExceptionNotFound`).join(',')} Exception{

           
            ${localEntity.name} instance = mapper.map(payload.value());
            Boolean exists = application.exists(instance.getInternalId());
            if (!exists){
                ${localEntity.relations.length > 0  ?`application.create (instance, payload.value());`:`application.create (instance);` }
                
            }
            
        }

    }
    `

}


import {expandToStringWithNL, Generated } from "langium";
import {LocalEntity, FunctionMapping } from "../../../language-server/generated/ast";

export function generateListenerMongo(localEntity: LocalEntity, ontology:String, topic_pattern: String, package_name: string): Generated {

    return expandToStringWithNL`

    package ${package_name}.listeners.mongo;

    import lombok.RequiredArgsConstructor;
    import lombok.extern.slf4j.Slf4j;
    import org.apache.kafka.clients.consumer.ConsumerRecord;
    import org.bson.Document;
    import org.springframework.beans.factory.annotation.Autowired;
    import org.springframework.data.mongodb.core.MongoTemplate;
    import org.springframework.kafka.annotation.KafkaListener;
    import org.springframework.stereotype.Service;

    @Slf4j
    @RequiredArgsConstructor
    @Service
    public class ${localEntity.name}InputListener {

        @Autowired
        private MongoTemplate mongoTemplate;

        @KafkaListener(topicPattern = "ontology.${ontology}.${localEntity.name.toLocaleLowerCase()}", groupId = "${localEntity.name.toLocaleLowerCase()}Input-${ontology}-group", concurrency = "2")
        public void consume(ConsumerRecord<String, String> payload) {

            try{    

                String data = payload.value();
                Document doc = Document.parse (data);
                mongoTemplate.insert(doc, "input");
    
            }catch (Exception e ){
                log.error(e.getMessage());
            }
        }
    }

    `
}

export function generateListener(localEntity: LocalEntity, topic_pattern: String, functionMapping: FunctionMapping, package_name: string): Generated {

    return expandToStringWithNL`

    package ${package_name}.listeners;
    import ${package_name}.services.${localEntity.name}Service;
    import ${package_name}.filters.${functionMapping.name}Filter;
    import ${package_name}.mappers.${functionMapping.name}Mapper;

    import lombok.RequiredArgsConstructor;
    import lombok.extern.slf4j.Slf4j;
    import org.apache.kafka.clients.consumer.ConsumerRecord;
    import org.springframework.beans.factory.annotation.Autowired;
    import org.springframework.beans.factory.annotation.Value;
    import org.springframework.kafka.annotation.KafkaListener;
    import org.springframework.stereotype.Service;
    import org.springframework.kafka.core.KafkaTemplate;

    @Slf4j
    @RequiredArgsConstructor
    @Service
    public class ${functionMapping.name}Listener {

        
        @Autowired
        private ${functionMapping.name}Filter filter;   
        
        @Autowired
        private ${functionMapping.name}Mapper mapper;   

        @Autowired
        private ${localEntity.name}Service service;      
        
        private final KafkaTemplate<String, String> kafkaTemplate;


        @KafkaListener(topics = "${topic_pattern.toLowerCase()}.${functionMapping.source.toLowerCase()}", groupId = "${functionMapping.name.toLowerCase()}-group", concurrency = "2")
        public void consume(ConsumerRecord<String, String> payload) {

            try{

                               
                String data = payload.value();

                if (filter.isValid(data)){
                    
                    this.service.process(payload, mapper);
                }

            }
            ${localEntity.relations.map(relation => generateCatchListener(relation.type.ref?.name ?? 'base',topic_pattern )).join("\n")}
            catch (Exception e ){
                log.error(e.getMessage());
            }
        }
    }
    `

}

export function generateCatchListener(entity: String, topic_pattern: String): Generated {
return expandToStringWithNL`
catch (${entity}ExceptionNotFound e){
    log.error(e.getMessage());
    kafkaTemplate.send("${topic_pattern.toLowerCase()}", payload.value());

}
`
}

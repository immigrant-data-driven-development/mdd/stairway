import {expandToStringWithNL, Generated, toString } from "langium";
import {LocalEntity, FunctionMapping, WhenMapper } from "../../../language-server/generated/ast";

export function generateFilter(localEntity: LocalEntity, functionMapping: FunctionMapping, package_name: string): Generated {

    return expandToStringWithNL`
    package ${package_name}.filters;

    import com.fasterxml.jackson.databind.JsonNode;
    import com.fasterxml.jackson.databind.ObjectMapper;
    import org.springframework.stereotype.Component;

    import java.util.Map;

    @Component
    public class ${functionMapping.name}Filter {

    public Boolean isValid (String element) throws Exception {

        ${toString(generateFilterConditions(functionMapping))}
    }
}`

}

function generateFilterConditions (functionMapping: FunctionMapping):Generated{
    
    return expandToStringWithNL`
    ${functionMapping.whenmappers.length? generateExpression(functionMapping) : `return true;`}
    `
}

function generateExpression(functionMapping: FunctionMapping):Generated{
    return expandToStringWithNL`
    ObjectMapper objectMapper = new ObjectMapper();

    JsonNode rootNode = objectMapper.readTree(element);

    return (${functionMapping.whenmappers.map( when => generateCondition(when)).join(`&&\n`)})? true : false;`
}

function generateCondition (whenMapper: WhenMapper): Generated {
    return expandToStringWithNL`
rootNode.${whenMapper.source.split(".").flatMap(element => `path("${element}")`).join(".")}.asText().equals("${whenMapper.value}")`
}




        

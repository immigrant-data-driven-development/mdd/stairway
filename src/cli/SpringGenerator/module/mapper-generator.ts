import {expandToStringWithNL, expandToString, Generated } from "langium";
import {LocalEntity, FunctionMapping, AttributesMapper } from "../../../language-server/generated/ast";

export function generateMapper(localEntity: LocalEntity, functionMapping: FunctionMapping, package_name: string, util_package: string): Generated {

    return expandToStringWithNL`
    package ${package_name}.mappers;

    import ${localEntity.ontology?.module}.${localEntity.name};
    import ${util_package}.Mapper;
    import ${util_package}.DateUtil;
    import ${util_package}.StringUtil;

    import org.springframework.boot.json.JsonParser;
    import org.springframework.boot.json.JsonParserFactory;
    import org.springframework.stereotype.Component;
    import java.time.LocalDateTime;
    import com.fasterxml.jackson.databind.JsonNode;
    import com.fasterxml.jackson.databind.ObjectMapper;

    @Component
    public class ${functionMapping.name}Mapper  implements Mapper <${localEntity.name}>{

    public ${localEntity.name} map (String element) throws Exception {

        ObjectMapper objectMapper = new ObjectMapper();

        JsonNode rootNode = objectMapper.readTree(element);

        ${functionMapping.attributemappers.map(att => createAttributesMappers(att)).join("\n")}
        
        return ${localEntity.name}.builder().${functionMapping.attributemappers.map(att => `${att.target.ref?.name.toLocaleLowerCase()}(${att.target.ref?.name.toLocaleLowerCase()})`).join(".\n")}.build();
    }
}`

}

function createAttributesMappers (att: AttributesMapper): Generated {
    return expandToStringWithNL`
    ${createAttributesType(att)} ${att.target.ref?.name.toLocaleLowerCase()} ${parserMapping(att)}
    `
}

function entireCommnad(att: AttributesMapper): String {
    return expandToString `rootNode.${att.source.split(".").flatMap(element => `path("${element}")`).join(".")}.asText()`
}

function createAttributesType (att: AttributesMapper): String{
    
    
    switch(att.target.ref?.type.toLowerCase()) { 
        case "date": { 
           return "LocalDateTime"
           
        }
        case "long": { 
            return "Long"
            
         }  
        case "integer": { 
           return "Integer"
           
        } 
        default: { 
           return "String" 
           
        } 
     }
}
     
     function parserMapping (att: AttributesMapper): String{
    
    
        switch(att.target.ref?.type.toLowerCase()) { 
            case "date": { 
               return expandToString` = DateUtil.createLocalDateTimeZ(${entireCommnad(att)});`
               
            }
            case "long": { 
                return expandToString`= Long.parseLong(${entireCommnad(att)});`
                
             }  
            case "integer": { 
               return expandToString` = Integer.parseInt(${entireCommnad(att)});`
               
            } 
            default: { 
               return expandToString` = StringUtil.check(${entireCommnad(att)});`
            } 
         }
}

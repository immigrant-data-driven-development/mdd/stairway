import fs from "fs";
import {  expandToStringWithNL } from "langium";
import { createPath, base_ident } from '../util/generator-utils'
import { Application,Configuration, isEnumX, Module, isModule,FunctionMapping, LocalEntity, isLocalEntity, Relation, isOneToOne, isManyToMany, isManyToOne
 } from "../../language-server/generated/ast";
import path from 'path'

const ident = base_ident

export function generateDocumentation(model: Application, target_folder: string) : void {
    fs.mkdirSync(target_folder, {recursive:true})
  
    if (model.configuration){
        fs.writeFileSync(path.join(target_folder, 'README.md'),createProjectReadme(model.configuration))
    }

    const DOCS_PATH = createPath(target_folder, "docs")
    const modules = model.abstractElements.filter(isModule)
    
    fs.writeFileSync(path.join(DOCS_PATH, "README.md"), generalREADME(modules))
    fs.writeFileSync(path.join(DOCS_PATH, "packagediagram.puml"), createPackageDiagram(modules, model.configuration?.software_name))

    for (const m of modules) {
        const MODULE_PATH = createPath(DOCS_PATH, m.name.toLowerCase())
        fs.writeFileSync(path.join(MODULE_PATH, "/README.md"), moduleREADME(m))
        fs.writeFileSync(path.join(MODULE_PATH, "/classdiagram.puml"), createClassDiagram(m))
    }

  
}

function createPackageDiagram(modules: Module[], name?: string) : string {
    const lines = [
        `@startuml ${name ?? ''}`,
        ...modules.flatMap(m => [
            `namespace ${m.name} {`,
            ``,
            `}`,
        ]),
        `@enduml`,
        ``
    ]

    return lines.join('\n')
}


function createClassDiagram(m: Module) : string {
    const enums = m.elements.filter(isEnumX)
    const entities = m.elements.filter(isLocalEntity)

    const lines = [
        `@startuml ${m.name}`,
        ...enums.flatMap(e => [
            `enum ${e.name} {`,
            ...e.attributes.map(a => `${ident}${a.name}`),
            `}`,
        ]),
        ``,
        ...entities.map(e => entityClassDiagram(e, m)),
        `@enduml`,
        ``
    ]

    return lines.join('\n')
}

function entityClassDiagram(e: LocalEntity, m: Module) : string {
    const lines = [
        `class ${e.name} {`,
        ...e.attributes.map(a =>
            `${a.type}: ${a.name}`
        ),
        ``,
        ...e.relations.filter(r => !isManyToMany(r)).map(r =>
            `${r.type.ref?.name}: ${r.name.toLowerCase()}`
        ),
        `}`,
        e.superType?.ref ? `\n${e.superType.ref.name} <|-- ${e.name}\n` : '',
        e.enumentityatributes.map(a =>
            `${e.name} "1" -- "1" ${a.type.ref?.name} : ${a.name.toLowerCase()}>`,
        ),
        ...e.relations.filter(r => !isManyToOne(r)).map(r => relationDiagram(r, e, m)),
        ``
    ]

    return lines.join('\n')
}

function relationDiagram(r: Relation, e: LocalEntity, m: Module) : string {
    // Cardinalidades
    const tgt_card = isOneToOne(r)   ? "1" : "0..*"
    const src_card = isManyToMany(r) ? "0..*" : "1"
    // Módulo de origem da entidade destino
    const origin_module = r.type.ref?.$container.name.toLowerCase() !== m.name.toLowerCase() ?
        `${r.type.ref?.$container.name}.` :
        ""

    return `${e.name} "${src_card}" -- "${tgt_card}" ${origin_module}${r.type.ref?.name} : ${r.name.toLowerCase()} >`
}


function moduleREADME(m: Module) : string {
    
    return expandToStringWithNL`
    # 📕Documentation: ${m.name}
    ${m.description ?? ''}
    
    ## 🌀 Package's Data Model
    ![Domain Diagram](classdiagram.png)

    ### ⚡Entities

    ${m.elements.filter(isLocalEntity).map(e => `* **${e.name}** : ${e.description ?? '-'}` ).join('\n')}

    ## ✒️ Mapping Rules
    ${m.elements.filter(isLocalEntity).map(e => e.functions.length ? generateMappingRulesTable(e) : undefined ).join('\n')}
`    
}
    
//?? generateMappingRulesTable(e)
function generateMappingRulesTable (e: LocalEntity): String {
    return expandToStringWithNL`
    ### ${e.name}
    ${e.description}

    ${e.functions.map(f => generateMappingRules(f,e)).join('\n')}
    `
}

function generateMappingRules (f: FunctionMapping, e: LocalEntity): String {
    return expandToStringWithNL`
    #### From ${f.source} to ${e.name}
    
    ${f.attributemappers.map(a => `* *${a.source}* **equal** *${a.target.ref?.name}*`).join('\n')}

    ${f.whenmappers.map(w => `**When** *${w.source}* **equal** *${w.value}*`).join(`\n`)}
    `
}

function generalREADME(modules: Module[]) : string {
    return expandToStringWithNL`
    # 📕Documentation

    ## 🌀 Project's Package Model
    ![Domain Diagram](packagediagram.png)

    ### 📲 Modules
    ${modules.map(m => `* **[${m.name}](./${m.name.toLocaleLowerCase()}/)** :${m.description ?? '-'}`).join("\n")}

    `
}

function stackREADME (stack: string): string {
    if (stack == "springboot")
    {
        return expandToStringWithNL`
        1. Spring Boot 3.0
        2. Spring Kafka                
        `
    } 
    return expandToStringWithNL`
    1. Flink 1.16    
    `

}

function createProjectReadme(configuration: Configuration): string{
    return expandToStringWithNL`
    
    # ${configuration.software_name}
    ## 🚀 Goal
    
    ${configuration.about}

    ## 📕 Domain Documentation
    
    Domain documentation can be found [here](./docs/README.md)

    ## ⚙️ Requirements

    1. Postgresql
    2. Java 17
    3. Maven

    ## ⚙️ Stack 
    
    ${stackREADME(configuration.framework)}

    ## 🔧 Install

    TODO

    \`\`\`bash
    mvn Spring-boot:run 
    \`\`\`

    
    ## ✒️ Team
    
    * **[${configuration.author}](${configuration.author_email})**
    
    ## 📕 Literature

    `
}
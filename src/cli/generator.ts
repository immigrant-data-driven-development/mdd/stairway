import path from 'path';
import { Application } from '../language-server/generated/ast';
import { generateMainFlink } from './FlinkGenerator/main-generator'
import {generateMainSpring} from './SpringGenerator/main-generator'
import { generateDocumentation } from "./DocumentationGenerator/main-generator"

export function generateJavaScript(application: Application, filePath: string, destination: string | undefined): string {
    const final_destination = extractDestination(filePath, destination)
    if (application.configuration){
        console.log(application.configuration?.framework)
        if (application.configuration?.framework === 'flink'){
            generateMainFlink(application, final_destination)
        }
        else {
            
            generateMainSpring(application, final_destination)
        }
        
    }
    generateDocumentation(application,final_destination)
    return final_destination
}

function extractDestination(filePath: string, destination?: string) : string {
    const path_ext = new RegExp(path.extname(filePath)+'$', 'g')
    filePath = filePath.replace(path_ext, '')
  
    return destination ?? path.join(path.dirname(filePath), "generated")
  }